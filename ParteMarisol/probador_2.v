//`timescale 1s/100us
module probador_2 (
    input [31:0] data_out,
    input valid_out,
    input clk_4f,
    input clk_2f,
    input clk_f,
    output reg [7:0] data_in,
    output reg valid_0,
    output reg reset_L,
    output reg reset_L8_32,
    output reg clk_32f);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;

        // $display ("\t\t\tclk_32f");
        // $monitor($time,"\t%b", clk_32f);

        @(posedge clk_32f);
        reset_L <= 0;
        reset_L8_32 <= 0;
        valid_0 <= 0;
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        reset_L <= 1;
        //valid_0 <= 1;

        @(posedge clk_4f);
        data_in  <= 8'hAB;
        reset_L8_32 <= 1;
        valid_0 <= 1;
        // reset_L <= 1;

        @(posedge clk_4f);
        data_in <= 8'hCD;

        @(posedge clk_4f);
        data_in <= 8'hEF; 

        @(posedge clk_4f);
        data_in <= 8'h45; 

        @(posedge clk_4f);
        data_in <= 8'h12; 
        //valid_0 <= 0;

        @(posedge clk_4f);
        data_in <= 8'h34; 

        @(posedge clk_4f);
        data_in <= 8'h56; 

        @(posedge clk_4f);
        data_in <= 8'h78; 

        @(posedge clk_4f);
        data_in <= 8'h9A;
        //valid_0 <= 1;

        @(posedge clk_4f);
        data_in <= 8'h5b;

        @(posedge clk_4f);
        data_in <= 8'h6C;

        @(posedge clk_4f);
        data_in <= 8'h8D;

        @(posedge clk_4f);
        valid_0 <= 0;


    #100 $finish;
    end

    initial	clk_32f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	  always	#2 clk_32f 	<= ~clk_32f;

endmodule 

