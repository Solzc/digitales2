module a8b_32b (
    output reg [31:0] data_out,
    output reg valid_out,
    input [7:0] data_in,
    input valid_0,
    input reset_L8_32,
    input clk_4f);

reg [3:0] i;
reg [31:0] temp;
reg [31:0] temp2;
reg [7:0] contador;
reg [31:0] store_data_out;
reg store_valid;

always @ (posedge clk_4f) begin 
    if (reset_L8_32 == 1) begin 

        if (valid_0 == 0) begin
            i <= 3;
            contador <= 0;
            store_valid <= 0;
        end

        if (valid_0 == 1) begin
            i <= i - 1; 
            contador <= contador + 1;

            if (contador == 3) begin
                contador <= 0;
                store_valid <= 1;
            end

        end

        if (i == 0)
            i <= 3;

        temp2[8*i +: 8] <= data_in;
        
        store_data_out <= data_out;

    end else begin
        i <= 3;
        contador <= 0;
        store_valid <= 0;
    end

end 

always @ (*) begin
    data_out = store_data_out;
    temp = temp2;
    valid_out = store_valid;
    
    if (valid_0 == 1) begin
        temp[8*i +: 8] = data_in;
        if  (contador == 3) begin
            data_out = temp;
            valid_out = 1;
        end

    end else begin
        valid_out = 0;
    end
end

endmodule