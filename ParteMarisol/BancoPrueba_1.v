`include "probador_1.v"
`include "a32b_8b.v"
`include "clocks.v"
`include "cmos_cells.v"
`include "clocks_s.v"
`include "a32b_8b_s.v"

module BancoPrueba_1;

    wire clk_32f, clk_4f, clk_2f, clk_f, clk_4f_1, reset_L, reset_L32_8;
	wire valid_0, valid_out, valid_out_s;
	wire [7:0] data_out, data_out_s;
	wire [31:0] data_in;
	wire clk_4f_s, clk_2f_s, clk_f_s;

    clocks clocks (/*AUTOINST*/
		   // Outputs
		   .clk_4f		(clk_4f),
		   .clk_2f		(clk_2f),
		   .clk_f		(clk_f),
		   // Inputs
		   .clk_32f		(clk_32f),
		   .reset_L		(reset_L));

	clocks_s clocks_s (/*AUTOINST*/
			   // Outputs
			   .clk_2f_s		(clk_2f_s),
			   .clk_4f_s		(clk_4f_s),
			   .clk_f_s		(clk_f_s),
			   // Inputs
			   .clk_32f		(clk_32f),
			   .reset_L		(reset_L));

	a32b_8b a32b_8b (/*AUTOINST*/
			 // Outputs
			 .data_out		(data_out[7:0]),
			 .valid_out		(valid_out),
			 // Inputs
			 .data_in		(data_in[31:0]),
			 .valid_0		(valid_0),
			 .reset_L32_8	(reset_L32_8),
			 .clk_4f		(clk_4f),
			 .clk_32f		(clk_32f));
	
	a32b_8b_s a32b_8b_s (/*AUTOINST*/
			     // Outputs
			     .data_out_s	(data_out_s[7:0]),
			     .valid_out_s	(valid_out_s),
			     // Inputs
			     .clk_4f		(clk_4f),
			     .data_in		(data_in[31:0]),
			     .reset_L32_8		(reset_L32_8),
			     .valid_0		(valid_0));

    probador_1 probador_1 (/*AUTOINST*/
			   // Outputs
			   .clk_32f		(clk_32f),
			   .data_in		(data_in[31:0]),
			   .valid_0		(valid_0),
			   .reset_L		(reset_L),
			   .reset_L32_8		(reset_L32_8),
			   // Inputs
			   .clk_4f		(clk_4f),
			   .clk_2f		(clk_2f),
			   .clk_f		(clk_f),
			   .clk_4f_s		(clk_4f_s),
			   .clk_2f_s		(clk_2f_s),
			   .clk_f_s		(clk_f_s),
			   .data_out		(data_out[7:0]),
			   .data_out_s		(data_out_s[7:0]),
			   .valid_out		(valid_out),
			   .valid_out_s		(valid_out_s));


endmodule 
