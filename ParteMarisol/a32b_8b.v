module a32b_8b (
    output reg [7:0] data_out,
    output reg valid_out,
    input [31:0] data_in,
    input valid_0,
    input reset_L32_8,
    input clk_4f,
    input clk_32f);


reg [3:0] i;

always @ (posedge clk_4f) begin 
    if (reset_L32_8 == 1 && valid_0 == 1) begin
        i <= i - 1; 

        if (i == 0) begin
            i <= 3;
        end    

    end else begin
        i <= 3;
    end
end 


always @ (*) begin
    data_out = data_in[8*i +: 8];
    valid_out = 0;

    if (reset_L32_8 == 1) begin
        if (valid_0 == 1) begin
            valid_out = 1;
        end else begin
            valid_out = 0;
            data_out = 0;
        end
    end 
end


endmodule