/* Generated by Yosys 0.5+ (git sha1 f13e387, gcc 5.3.1-8ubuntu2 -O2 -fstack-protector-strong -fPIC -Os) */

(* top =  1  *)
(* src = "clocks_s.v:1" *)
module clocks_s(clk_4f_s, clk_2f_s, clk_f_s, clk_32f, reset_L);
  (* src = "clocks_s.v:14" *)
  wire _000_;
  (* src = "clocks_s.v:14" *)
  wire _001_;
  (* src = "clocks_s.v:14" *)
  wire _002_;
  (* src = "clocks_s.v:14" *)
  wire [4:0] _003_;
  (* src = "clocks_s.v:14" *)
  wire [3:0] _004_;
  (* src = "clocks_s.v:14" *)
  wire [4:0] _005_;
  wire _006_;
  wire _007_;
  wire _008_;
  wire _009_;
  wire _010_;
  wire _011_;
  wire _012_;
  wire _013_;
  wire _014_;
  wire _015_;
  wire _016_;
  wire _017_;
  wire _018_;
  wire _019_;
  wire _020_;
  wire _021_;
  wire _022_;
  wire _023_;
  wire _024_;
  wire _025_;
  wire _026_;
  wire _027_;
  wire _028_;
  wire _029_;
  wire _030_;
  wire _031_;
  wire _032_;
  wire _033_;
  wire _034_;
  wire _035_;
  wire _036_;
  wire _037_;
  wire _038_;
  wire _039_;
  wire _040_;
  wire _041_;
  wire _042_;
  wire _043_;
  wire _044_;
  wire _045_;
  wire _046_;
  wire _047_;
  wire _048_;
  wire _049_;
  wire _050_;
  wire _051_;
  wire _052_;
  wire _053_;
  wire _054_;
  wire _055_;
  wire _056_;
  wire _057_;
  wire _058_;
  wire _059_;
  wire _060_;
  wire _061_;
  wire _062_;
  wire _063_;
  wire _064_;
  wire _065_;
  wire _066_;
  wire _067_;
  wire _068_;
  wire _069_;
  wire _070_;
  wire _071_;
  wire _072_;
  wire _073_;
  wire _074_;
  wire _075_;
  wire _076_;
  wire _077_;
  wire _078_;
  wire _079_;
  wire _080_;
  wire _081_;
  (* src = "clocks_s.v:3" *)
  output clk_2f_s;
  (* src = "clocks_s.v:5" *)
  input clk_32f;
  (* src = "clocks_s.v:2" *)
  output clk_4f_s;
  (* src = "clocks_s.v:4" *)
  output clk_f_s;
  (* src = "clocks_s.v:11" *)
  wire [4:0] contador_2f;
  (* src = "clocks_s.v:9" *)
  wire [3:0] contador_4f;
  (* src = "clocks_s.v:10" *)
  wire [4:0] contador_f;
  (* src = "clocks_s.v:6" *)
  input reset_L;
  NOT _082_ (
    .A(contador_4f[2]),
    .Y(_028_)
  );
  NOR _083_ (
    .A(contador_4f[1]),
    .B(contador_4f[0]),
    .Y(_029_)
  );
  NAND _084_ (
    .A(_029_),
    .B(_028_),
    .Y(_030_)
  );
  NOR _085_ (
    .A(_030_),
    .B(clk_4f_s),
    .Y(_031_)
  );
  NOT _086_ (
    .A(_029_),
    .Y(_032_)
  );
  NAND _087_ (
    .A(_032_),
    .B(contador_4f[2]),
    .Y(_033_)
  );
  NOT _088_ (
    .A(reset_L),
    .Y(_034_)
  );
  NOR _089_ (
    .A(contador_4f[3]),
    .B(_034_),
    .Y(_035_)
  );
  NAND _090_ (
    .A(_035_),
    .B(_033_),
    .Y(_036_)
  );
  NOR _091_ (
    .A(_036_),
    .B(_031_),
    .Y(_001_)
  );
  NOR _092_ (
    .A(contador_f[0]),
    .B(contador_f[1]),
    .Y(_037_)
  );
  NOR _093_ (
    .A(contador_f[3]),
    .B(contador_f[2]),
    .Y(_038_)
  );
  NAND _094_ (
    .A(_038_),
    .B(_037_),
    .Y(_039_)
  );
  NOR _095_ (
    .A(_039_),
    .B(contador_f[4]),
    .Y(_040_)
  );
  NOT _096_ (
    .A(contador_2f[3]),
    .Y(_041_)
  );
  NOT _097_ (
    .A(contador_2f[0]),
    .Y(_042_)
  );
  NOT _098_ (
    .A(contador_2f[1]),
    .Y(_043_)
  );
  NAND _099_ (
    .A(_043_),
    .B(_042_),
    .Y(_044_)
  );
  NOR _100_ (
    .A(_044_),
    .B(contador_2f[2]),
    .Y(_045_)
  );
  NOR _101_ (
    .A(_045_),
    .B(_041_),
    .Y(_046_)
  );
  NOR _102_ (
    .A(_046_),
    .B(contador_2f[4]),
    .Y(_047_)
  );
  NOR _103_ (
    .A(_047_),
    .B(_040_),
    .Y(_048_)
  );
  NOT _104_ (
    .A(clk_2f_s),
    .Y(_049_)
  );
  NAND _105_ (
    .A(_040_),
    .B(_049_),
    .Y(_050_)
  );
  NAND _106_ (
    .A(_050_),
    .B(reset_L),
    .Y(_051_)
  );
  NOR _107_ (
    .A(_051_),
    .B(_048_),
    .Y(_000_)
  );
  NOT _108_ (
    .A(contador_f[4]),
    .Y(_052_)
  );
  NOT _109_ (
    .A(_039_),
    .Y(_053_)
  );
  NOR _110_ (
    .A(_053_),
    .B(_052_),
    .Y(_054_)
  );
  NOT _111_ (
    .A(clk_f_s),
    .Y(_055_)
  );
  NAND _112_ (
    .A(_040_),
    .B(_055_),
    .Y(_056_)
  );
  NAND _113_ (
    .A(_056_),
    .B(reset_L),
    .Y(_057_)
  );
  NOR _114_ (
    .A(_057_),
    .B(_054_),
    .Y(_002_)
  );
  NOR _115_ (
    .A(contador_4f[0]),
    .B(_034_),
    .Y(_004_[0])
  );
  NAND _116_ (
    .A(contador_4f[1]),
    .B(contador_4f[0]),
    .Y(_058_)
  );
  NAND _117_ (
    .A(_058_),
    .B(reset_L),
    .Y(_059_)
  );
  NOR _118_ (
    .A(_059_),
    .B(_029_),
    .Y(_004_[1])
  );
  NOR _119_ (
    .A(_058_),
    .B(_028_),
    .Y(_060_)
  );
  NAND _120_ (
    .A(_058_),
    .B(_028_),
    .Y(_061_)
  );
  NAND _121_ (
    .A(_061_),
    .B(reset_L),
    .Y(_062_)
  );
  NOR _122_ (
    .A(_062_),
    .B(_060_),
    .Y(_004_[2])
  );
  NOT _123_ (
    .A(contador_4f[3]),
    .Y(_063_)
  );
  NOT _124_ (
    .A(_060_),
    .Y(_064_)
  );
  NAND _125_ (
    .A(_064_),
    .B(_063_),
    .Y(_065_)
  );
  NAND _126_ (
    .A(_065_),
    .B(_030_),
    .Y(_066_)
  );
  NAND _127_ (
    .A(_060_),
    .B(contador_4f[3]),
    .Y(_067_)
  );
  NAND _128_ (
    .A(_067_),
    .B(reset_L),
    .Y(_068_)
  );
  NOR _129_ (
    .A(_068_),
    .B(_066_),
    .Y(_004_[3])
  );
  NOR _130_ (
    .A(contador_f[0]),
    .B(_034_),
    .Y(_005_[0])
  );
  NOT _131_ (
    .A(contador_f[1]),
    .Y(_069_)
  );
  NOT _132_ (
    .A(contador_f[0]),
    .Y(_070_)
  );
  NOR _133_ (
    .A(_070_),
    .B(_069_),
    .Y(_071_)
  );
  NOT _134_ (
    .A(_071_),
    .Y(_072_)
  );
  NAND _135_ (
    .A(_072_),
    .B(reset_L),
    .Y(_073_)
  );
  NOR _136_ (
    .A(_073_),
    .B(_037_),
    .Y(_005_[1])
  );
  NAND _137_ (
    .A(_071_),
    .B(contador_f[2]),
    .Y(_074_)
  );
  NOT _138_ (
    .A(_074_),
    .Y(_075_)
  );
  NOT _139_ (
    .A(contador_f[2]),
    .Y(_076_)
  );
  NAND _140_ (
    .A(_072_),
    .B(_076_),
    .Y(_077_)
  );
  NAND _141_ (
    .A(_077_),
    .B(reset_L),
    .Y(_078_)
  );
  NOR _142_ (
    .A(_078_),
    .B(_075_),
    .Y(_005_[2])
  );
  NOT _143_ (
    .A(contador_f[3]),
    .Y(_079_)
  );
  NOR _144_ (
    .A(_074_),
    .B(_079_),
    .Y(_080_)
  );
  NAND _145_ (
    .A(_074_),
    .B(_079_),
    .Y(_081_)
  );
  NAND _146_ (
    .A(_081_),
    .B(reset_L),
    .Y(_006_)
  );
  NOR _147_ (
    .A(_006_),
    .B(_080_),
    .Y(_005_[3])
  );
  NOR _148_ (
    .A(_080_),
    .B(contador_f[4]),
    .Y(_007_)
  );
  NAND _149_ (
    .A(_080_),
    .B(contador_f[4]),
    .Y(_008_)
  );
  NAND _150_ (
    .A(_008_),
    .B(reset_L),
    .Y(_009_)
  );
  NOR _151_ (
    .A(_009_),
    .B(_007_),
    .Y(_005_[4])
  );
  NOR _152_ (
    .A(_034_),
    .B(contador_2f[0]),
    .Y(_003_[0])
  );
  NOR _153_ (
    .A(_043_),
    .B(_042_),
    .Y(_010_)
  );
  NAND _154_ (
    .A(_044_),
    .B(reset_L),
    .Y(_011_)
  );
  NOR _155_ (
    .A(_011_),
    .B(_010_),
    .Y(_003_[1])
  );
  NOT _156_ (
    .A(contador_2f[2]),
    .Y(_012_)
  );
  NAND _157_ (
    .A(contador_2f[1]),
    .B(contador_2f[0]),
    .Y(_013_)
  );
  NOR _158_ (
    .A(_013_),
    .B(_012_),
    .Y(_014_)
  );
  NAND _159_ (
    .A(_013_),
    .B(_012_),
    .Y(_015_)
  );
  NAND _160_ (
    .A(_015_),
    .B(reset_L),
    .Y(_016_)
  );
  NOR _161_ (
    .A(_016_),
    .B(_014_),
    .Y(_003_[2])
  );
  NAND _162_ (
    .A(_010_),
    .B(contador_2f[2]),
    .Y(_017_)
  );
  NOR _163_ (
    .A(_017_),
    .B(_041_),
    .Y(_018_)
  );
  NAND _164_ (
    .A(_017_),
    .B(_041_),
    .Y(_019_)
  );
  NAND _165_ (
    .A(_019_),
    .B(reset_L),
    .Y(_020_)
  );
  NOR _166_ (
    .A(_020_),
    .B(_018_),
    .Y(_003_[3])
  );
  NAND _167_ (
    .A(_045_),
    .B(_041_),
    .Y(_021_)
  );
  NAND _168_ (
    .A(_021_),
    .B(reset_L),
    .Y(_022_)
  );
  NOT _169_ (
    .A(contador_2f[4]),
    .Y(_023_)
  );
  NOR _170_ (
    .A(_018_),
    .B(_023_),
    .Y(_024_)
  );
  NAND _171_ (
    .A(_014_),
    .B(contador_2f[3]),
    .Y(_025_)
  );
  NOR _172_ (
    .A(_025_),
    .B(contador_2f[4]),
    .Y(_026_)
  );
  NOR _173_ (
    .A(_026_),
    .B(_024_),
    .Y(_027_)
  );
  NOR _174_ (
    .A(_027_),
    .B(_022_),
    .Y(_003_[4])
  );
  DFF _175_ (
    .C(clk_32f),
    .D(_001_),
    .Q(clk_4f_s)
  );
  DFF _176_ (
    .C(clk_32f),
    .D(_000_),
    .Q(clk_2f_s)
  );
  DFF _177_ (
    .C(clk_32f),
    .D(_002_),
    .Q(clk_f_s)
  );
  DFF _178_ (
    .C(clk_32f),
    .D(_004_[0]),
    .Q(contador_4f[0])
  );
  DFF _179_ (
    .C(clk_32f),
    .D(_004_[1]),
    .Q(contador_4f[1])
  );
  DFF _180_ (
    .C(clk_32f),
    .D(_004_[2]),
    .Q(contador_4f[2])
  );
  DFF _181_ (
    .C(clk_32f),
    .D(_004_[3]),
    .Q(contador_4f[3])
  );
  DFF _182_ (
    .C(clk_32f),
    .D(_005_[0]),
    .Q(contador_f[0])
  );
  DFF _183_ (
    .C(clk_32f),
    .D(_005_[1]),
    .Q(contador_f[1])
  );
  DFF _184_ (
    .C(clk_32f),
    .D(_005_[2]),
    .Q(contador_f[2])
  );
  DFF _185_ (
    .C(clk_32f),
    .D(_005_[3]),
    .Q(contador_f[3])
  );
  DFF _186_ (
    .C(clk_32f),
    .D(_005_[4]),
    .Q(contador_f[4])
  );
  DFF _187_ (
    .C(clk_32f),
    .D(_003_[0]),
    .Q(contador_2f[0])
  );
  DFF _188_ (
    .C(clk_32f),
    .D(_003_[1]),
    .Q(contador_2f[1])
  );
  DFF _189_ (
    .C(clk_32f),
    .D(_003_[2]),
    .Q(contador_2f[2])
  );
  DFF _190_ (
    .C(clk_32f),
    .D(_003_[3]),
    .Q(contador_2f[3])
  );
  DFF _191_ (
    .C(clk_32f),
    .D(_003_[4]),
    .Q(contador_2f[4])
  );
endmodule
