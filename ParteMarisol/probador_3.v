module probador_3(
input [7:0] data_out,
input active, 
input valid_out,
input clk_4f,
input clk_2f,
input clk_f,
input active_s, 
input [7:0] data_out_s, 
input valid_out_s,
output reg reset_L,
output reg reset_stp,
output reg clk_32f,  
output reg data_in);

 
initial begin 
    $dumpfile("out1.vcd");
    $dumpvars;

  //  data_in <= 1'b0; 
    @(posedge clk_32f);
    reset_L <= 0;
    reset_stp <= 0;

    @(posedge clk_32f);
    reset_L <= 1;
    reset_stp <= 1;
    data_in <= 1'b1; 

    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b0; 

    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b0; 

    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b0; 

    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b0;
    
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 

    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0;

    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0;

    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b0; 

    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0; 
    @(posedge clk_32f);
    data_in <= 1'b1; 
    @(posedge clk_32f);
    data_in <= 1'b0;

    #70 
    $finish;

end

  initial	clk_32f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	always	#2 clk_32f 	<= ~clk_32f;


endmodule