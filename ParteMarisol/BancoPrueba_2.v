`include "probador_2.v"
`include "a8b_32b.v"
`include "a8b_32b_s.v"
`include "clocks.v"
`include "cmos_cells.v"

module BancoPrueba_2;

    wire [31:0] data_out, data_out_s;
    wire valid_out, valid_out_s;
    wire [7:0] data_in;
    wire valid_0;
	wire clk_32f, clk_4f, clk_2f, clk_f, clk_4f_1, reset_L, resetL8_32;

	clocks clocks (/*AUTOINST*/
		       // Outputs
		       .clk_4f		(clk_4f),
		       .clk_2f		(clk_2f),
		       .clk_f		(clk_f),
		       // Inputs
		       .clk_32f		(clk_32f),
		       .reset_L		(reset_L));

	a8b_32b a8b_32b (/*AUTOINST*/
			 // Outputs
			 .data_out		(data_out[31:0]),
			 .valid_out		(valid_out),
			 // Inputs
			 .data_in		(data_in[7:0]),
			 .valid_0		(valid_0),
			 .reset_L8_32		(reset_L8_32),
			 .clk_4f		(clk_4f));

	a8b_32b_s a8b_32b_s (/*AUTOINST*/
			     // Outputs
			     .data_out_s	(data_out_s[31:0]),
			     .valid_out_s	(valid_out_s),
			     // Inputs
			     .clk_4f		(clk_4f),
			     .data_in		(data_in[7:0]),
			     .reset_L8_32	(reset_L8_32),
			     .valid_0		(valid_0));

    probador_2 probador_2 (/*AUTOINST*/
			   // Outputs
			   .data_in		(data_in[7:0]),
			   .valid_0		(valid_0),
			   .reset_L		(reset_L),
			   .reset_L8_32		(reset_L8_32),
			   .clk_32f		(clk_32f),
			   // Inputs
			   .data_out		(data_out[31:0]),
			   .valid_out		(valid_out),
			   .clk_4f		(clk_4f),
			   .clk_2f		(clk_2f),
			   .clk_f		(clk_f));


endmodule 
