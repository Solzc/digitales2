//`timescale 1s/100us
module probador_1 (
    output reg clk_32f,
    output reg [31:0] data_in,
    output reg valid_0,
    output reg reset_L,
    output reg reset_L32_8,
    input clk_4f,
    input clk_2f,
    input clk_f,
    input clk_4f_s,
    input clk_2f_s,
    input clk_f_s,
    input [7:0] data_out,
    input [7:0] data_out_s,
    input valid_out,
    input valid_out_s);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;

        // $display ("\t\t\tclk_32f");
        // $monitor($time,"\t%b", clk_32f);

        @(posedge clk_32f);
        reset_L <= 0;
        reset_L32_8 <= 0;
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        reset_L <= 1;
        valid_0 <= 1;

        @(posedge clk_f);
        data_in <= 32'hABCDE123;
        reset_L32_8 <= 1;

        @(posedge clk_f);
        data_in <= 32'h45DF23EA; 
       // valid_0 <= 0;

        @(posedge clk_f);
        data_in <= 32'h12345678; 
      //  valid_0 <= 1;

        @(posedge clk_f);
        data_in <= 32'h87654321; 

        @(posedge clk_f);
        data_in <= 32'hABCDEFAB;

    $finish;
    end

    initial	clk_32f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	always	#2 clk_32f 	<= ~clk_32f;

endmodule 

