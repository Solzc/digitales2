module a8b_32b (
    output reg [31:0] data_out,
    output reg valid_out,
    input [7:0] data_in,
    input valid_0,
    input reset_L8_32,
    input clk_4f,
    input clk_f);

reg [3:0] i;
reg [31:0] temp;
reg [31:0] temp2;

always @ (posedge clk_4f) begin 
    if (reset_L8_32 == 1 && valid_0 == 1) begin 
        i <= i - 1; 

        if (i == 0)
            i <= 3;

        temp2[8*i +: 8] <= data_in;

    end else begin
        i <= 3;
    end

end 

always @ (posedge clk_f) begin
    if (valid_0 == 1) begin
        valid_out <= 1;
        if (i == 0)
            data_out <= temp;
    end else begin
        valid_out <= 0;
    end
end

always @ (*) begin
    temp = temp2;
    
    if (valid_0 == 1) begin
        temp[8*i +: 8] = data_in;
        valid_out = 1;
    end else begin
        valid_out = 0;
    end
end


endmodule