`include "probador_tx.v"
`include "phy_tx.v"
`include "clocks.v"

module BancoPrueba_tx;

    wire data_out_lane0, data_out_lane1, clk_32f, clk_4f, clk_2f, clk_f;
	wire reset_L_bs, reset_L32_8, reset_L_pts, reset_L, valid_in;
    wire [31:0] data_in;

	clocks clocks (/*AUTOINST*/
		       // Outputs
		       .clk_4f		(clk_4f),
		       .clk_2f		(clk_2f),
		       .clk_f		(clk_f),
		       // Inputs
		       .clk_32f		(clk_32f),
		       .reset_L		(reset_L));

	phy_tx phy_tx (/*AUTOINST*/
		       // Outputs
		       .data_out_lane0	(data_out_lane0),
		       .data_out_lane1	(data_out_lane1),
		       // Inputs
		       .clk_32f		(clk_32f),
		       .clk_4f		(clk_4f),
		       .clk_2f		(clk_2f),
		       .clk_f		(clk_f),
		       .reset_L_bs	(reset_L_bs),
		       .reset_L32_8	(reset_L32_8),
		       .reset_L_pts	(reset_L_pts),
		       .data_in		(data_in[31:0]),
		       .valid_in	(valid_in));

	probador_tx probador_tx (/*AUTOINST*/
			   // Outputs
			   .clk_32f		(clk_32f),
			   .reset_L_bs		(reset_L_bs),
			   .reset_L32_8		(reset_L32_8),
			   .reset_L_pts		(reset_L_pts),
			   .reset_L		(reset_L),
			   .data_in		(data_in[31:0]),
			   .valid_in		(valid_in),
			   // Inputs
			   .data_out_lane0	(data_out_lane0),
			   .data_out_lane1	(data_out_lane1),
			   .clk_4f		(clk_4f),
			   .clk_2f		(clk_2f),
			   .clk_f		(clk_f));


endmodule 
