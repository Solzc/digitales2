`include "probador_rx.v"
`include "phy_rx.v"
`include "clocks.v"

module BancoPrueba_rx;

    wire [31:0] data_out;
    wire valid_out, clk_4f, clk_2f, clk_f, clk_32f, reset_L;
    wire reset_stp, reset_L8_32, reset_L_us, data_in_lane0, data_in_lane1, valid_0, valid_1;

	clocks clocks (/*AUTOINST*/
		       // Outputs
		       .clk_4f		(clk_4f),
		       .clk_2f		(clk_2f),
		       .clk_f		(clk_f),
		       // Inputs
		       .clk_32f		(clk_32f),
		       .reset_L		(reset_L));

	phy_rx phy_rx (/*AUTOINST*/
		       // Outputs
		       .data_out	(data_out[31:0]),
		       .valid_out	(valid_out),
		       // Inputs
		       .clk_32f		(clk_32f),
		       .clk_4f		(clk_4f),
		       .clk_2f		(clk_2f),
		       .clk_f		(clk_f),
		       .reset_stp	(reset_stp),
		       .reset_L8_32	(reset_L8_32),
		       .reset_L_us	(reset_L_us),
		       .data_in_lane0	(data_in_lane0),
		       .data_in_lane1	(data_in_lane1),
		       .valid_0		(valid_0),
		       .valid_1		(valid_1));

	probador_rx probador_rx (/*AUTOINST*/
				 // Outputs
				 .clk_32f		(clk_32f),
				 .reset_stp		(reset_stp),
				 .reset_L8_32		(reset_L8_32),
				 .reset_L_us		(reset_L_us),
				 .reset_L		(reset_L),
				 .data_in_lane0		(data_in_lane0),
				 .data_in_lane1		(data_in_lane1),
				 .valid_0		(valid_0),
				 .valid_1		(valid_1),
				 // Inputs
				 .data_out		(data_out[31:0]),
				 .valid_out		(valid_out),
				 .clk_4f		(clk_4f),
				 .clk_2f		(clk_2f),
				 .clk_f			(clk_f));


endmodule 
