//`timescale 1s/100us
module probador_tx (
    input data_out_lane0,
    input data_out_lane1,
    input clk_4f,
    input clk_2f,
    input clk_f,
    output reg clk_32f,
    output reg reset_L_bs,
    output reg reset_L32_8,
    output reg reset_L_pts,
    output reg reset_L,
    output reg [31:0] data_in,
    output reg valid_in);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;

        $display ("\t\t\tclk_32f");
        $monitor($time,"\t%b", clk_32f);

        @(posedge clk_32f);
        valid_in <= 0;
        reset_L <= 0;
        reset_L_bs <= 0;
        reset_L32_8 <= 0;
        reset_L_pts <= 0;
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        reset_L <= 1;
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        reset_L_bs <= 1;
        reset_L32_8 <= 1;
        reset_L_pts <= 1;


        @(posedge clk_2f);
        data_in <= 32'hABCDE123;
        valid_in <= 1;

        @(posedge clk_2f);
        data_in <= 32'h45DF23EA; 

        @(posedge clk_2f);
        data_in <= 32'h12345678; 

        @(posedge clk_2f);
        data_in <= 32'h87654321; 

        @(posedge clk_2f);
        data_in <= 32'hABCDEFAB;



    $finish;
    end

    initial	clk_32f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	  always	#2 clk_32f 	<= ~clk_32f;

endmodule 

