//`timescale 1s/100us
module probador_rx (
    input [31:0] data_out,
    input valid_out,
    input clk_4f,
    input clk_2f,
    input clk_f,
    output reg clk_32f,
    output reg reset_stp,
    output reg reset_L8_32,
    output reg reset_L_us,
    output reg reset_L,
    output reg data_in_lane0,
    output reg data_in_lane1,
    output reg valid_0,
    output reg valid_1);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;

        // $display ("\t\t\tclk_32f");
        // $monitor($time,"\t%b", clk_32f);

        @(posedge clk_32f);
        valid_0 <= 0;
        valid_1 <= 0;
        reset_L <= 0;
        reset_stp <= 0;
        reset_L8_32 <= 0;
        reset_L_us <= 0;

        @(posedge clk_32f);
        reset_L <= 1;
        @(posedge clk_32f);

        @(posedge clk_32f);
        valid_0 <= 1;
        valid_1 <= 1;
        reset_stp <= 1;
        reset_L_us <= 1;
        reset_L8_32 <= 1;
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0;
        
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0;

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0;

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 

        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b1;
        data_in_lane1 <= 1'b1; 
        @(posedge clk_32f);
        data_in_lane0 <= 1'b0;
        data_in_lane1 <= 1'b0;

    #70 $finish;
    end

    initial	clk_32f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	  always	#2 clk_32f 	<= ~clk_32f;

endmodule 

