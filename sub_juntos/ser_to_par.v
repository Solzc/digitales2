module ser_to_par(
    input clk_4f,
    input clk_32f,  
    input data_in, 
    input reset_stp,
    output reg active, 
    output reg [7:0] data_out, 
    output reg valid_out); 

reg [3:0] BC_counter;
reg [3:0] i; 
reg [7:0] temp;
reg [7:0] temp2;
reg [7:0] temp3;
reg [7:0] data2send;
//reg temp_active;


always @(posedge clk_32f)begin
    if (reset_stp == 1) begin 
        i <= i-1;

        if (i == 0) begin
            i <= 7;
            temp2 = temp;
        end 

        temp3[1*i +: 1] <= data_in;

    end else begin
        i <= 7;
        BC_counter <= 0;
        active <= 0;
    end
end 

always @(posedge clk_4f)begin 

    if (temp2 == 8'hBC)begin
        BC_counter <= BC_counter + 1;
        valid_out <= 0;
    end else begin
        data_out <= temp2;
        BC_counter <= 0;
        if (active == 1)
            valid_out <= 1;
    end

    // if (BC_counter == 4) begin
    //     active <= 1;
    // end

    data2send <= temp2;
end
    
always @(*)begin 

    temp = temp3;

    if (reset_stp == 1) begin
        temp[1*i +:1] = data_in;
        
    end

    if (BC_counter == 4)begin
        active = 1;
    end 

end



endmodule 
