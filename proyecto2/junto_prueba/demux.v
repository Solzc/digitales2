module demux(clk,
    reset_L,
    Data_in,
    sel,
    Data_out_0,
    Data_out_1,
    Data_out_2,
    Data_out_3
    );

//list the inputs and their sizes
    input clk, reset_L;
    input [15:0] Data_in;
    input [1:0] sel;
//list the outputs and their sizes 
    output reg [15:0] Data_out_0;
    output reg [15:0] Data_out_1;
    output reg [15:0] Data_out_2;
    output reg [15:0] Data_out_3;
//Internal variables
    // reg Data_out_0;
    // reg Data_out_1;
    // reg Data_out_2;
    // reg Data_out_3;  

    // reg [15:0] temp0 = 4'h0;
    // reg [15:0] temp1 = 4'h0;
    // reg [15:0] temp2 = 4'h0;
    // reg [15:0] temp3 = 4'h0;
always @(posedge clk)begin

    if (reset_L == 0)begin 
        Data_out_0 <= 0;
        Data_out_1 <= 0;
        Data_out_2 <= 0;
        Data_out_3 <= 0;
        

    end




end

//always block with Data_in and sel in its sensitivity list
    always @(Data_in or sel)

    begin
        case (sel)  //case statement with "sel"
        //multiple statements can be written inside each case.
        //you just have to use 'begin' and 'end' keywords as shown below.
            2'b00 : begin
                        Data_out_0 = Data_in;
                      end
            2'b01 : begin
                        Data_out_1 = Data_in;
                      end
            2'b10 : begin
                        Data_out_2 = Data_in;
                      end
            2'b11 : begin
                        Data_out_3 = Data_in;
                      end
        endcase
    end
    
endmodule