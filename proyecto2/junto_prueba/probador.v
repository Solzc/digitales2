module probador(
output reg clk, 
output reg reset, 
output reg reset_L, 
output reg reset_salida,
output reg [15:0] data_in, 
output reg write, 
output reg read0_salida,
output reg read1_salida,
output reg read2_salida,
output reg read3_salida,
output reg req,
output reg [1:0] idx,
output reg reset_Lc,
input [15:0] data_out0,
input [15:0] data_out1,
input [15:0] data_out2,
input [15:0] data_out3,
input empty0_salida,
input empty1_salida,
input empty2_salida,
input empty3_salida,
input [2:0] total,
input valid, 
input [15:0] data_out0_s,
input [15:0] data_out1_s,
input [15:0] data_out2_s,
input [15:0] data_out3_s,
input empty0_salida_s,
input empty1_salida_s,
input empty2_salida_s,
input empty3_salida_s,
input [2:0] total_s,
input valid_s); 

    initial begin 
    $dumpfile("out.vcd");
    $dumpvars;

    reset <= 1'b1;
    write <= 0;
    reset_L <= 1'b0; 
    reset_Lc <= 1'b0; 
    req <= 0;
    reset_salida <= 1'b1;
    read0_salida <= 0;
    read1_salida <= 0;
    read2_salida <= 0;
    read3_salida <= 0;
    @(posedge clk);
        reset <= 1'b0; 
        reset_L <= 1'b0;
        reset_Lc <= 1'b1; 
        write <= 1;
        data_in <= 16'h5555;
     @(posedge clk);
        reset <= 1'b0; 
        reset_L <= 1'b0;
        write <= 1;
        data_in <= 16'hFFFF;
    @(posedge clk);
        reset <= 1'b0; 
        reset_L <= 1'b0;
        write <= 1;
        data_in <= 16'h0000;
    @(posedge clk);
        reset <= 1'b0; 
        reset_L <= 1'b0;
        write <= 1;
        data_in <= 16'hAAAA;
    @(posedge clk);
        write <= 0; 
        reset_L <= 1'b1;
        reset_salida <= 1'b0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    read0_salida <= 1;
    read1_salida <= 1;
    read2_salida <= 1;
    read3_salida <= 1;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
        req <= 1;
        idx <= 2;
    @(posedge clk);
    req <= 0;

    
     #250 
    $finish;

    end 

    initial	clk 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	always	#2 clk 	<= ~clk;

    
    







endmodule

