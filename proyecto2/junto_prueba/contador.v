module contador (  
    input clk,
    input req,
    input [1:0] idx,
    input reset_Lc,
    input write0,
    input write1,
    input write2,
    input write3,
    output reg [2:0] total,
    output reg valid);

reg [2:0] contador0;
reg [2:0] contador1;
reg [2:0] contador2;
reg [2:0] contador3;

always @(posedge clk) begin
    
    if (reset_Lc == 0) begin
        contador0 <= 0;
        contador1 <= 0;
        contador2 <= 0;
        contador3 <= 0;
        valid <= 0;
    end else begin
        valid <= 0;
        if (write0 == 1) begin
            contador0 <= contador0 + 1;
        end 
        
        if (write1 == 1) begin
            contador1 <= contador1 + 1;
        end 
        
        if (write2 == 1) begin
            contador2 <= contador2 + 1;
        end 
        
        if (write3 == 1) begin
            contador3 <= contador3 + 1;
        end
    end

    if (req == 1) begin
        if (idx == 0) begin
            total <= contador0;
            valid <= 1;
        end else if (idx == 1) begin
            total <= contador1;
            valid <= 1;
        end else if (idx == 2) begin 
            total <= contador2;
            valid <= 1;
        end else if (idx == 3) begin
            total <= contador3;
            valid <= 1;
        end
    end else begin
        total <= 0;
    end 

end

endmodule