`include "prueba.v"
`include "prueba_s.v"
`include "cmos_cells.v"
`include "probador.v"
module bancoprueba(); 

wire clk, reset, reset_L, reset_salida, write,  read0_salida, read1_salida, read2_salida, read3_salida, empty0_salida, empty1_salida, empty2_salida, empty3_salida,
 empty0_salida_s, empty1_salida_s, empty2_salida_s, empty3_salida_s;
// wire [1:0] dest, s_mux; 
wire [15:0] data_in,data_out0,data_out1,data_out2,data_out3,data_out0_s,data_out1_s,data_out2_s,data_out3_s; 
wire [1:0] idx;
wire [2:0] total, total_s;
wire req, reset_Lc, valid, valid_s;

prueba prueba(/*AUTOINST*/
	      // Outputs
	      .data_out0		(data_out0[15:0]),
	      .data_out1		(data_out1[15:0]),
	      .data_out2		(data_out2[15:0]),
	      .data_out3		(data_out3[15:0]),
	      .empty0_salida		(empty0_salida),
	      .empty1_salida		(empty1_salida),
	      .empty2_salida		(empty2_salida),
	      .empty3_salida		(empty3_salida),
	      .total			(total[2:0]),
	      .valid			(valid),
	      // Inputs
	      .clk			(clk),
	      .reset			(reset),
	      .reset_L			(reset_L),
	      .reset_salida		(reset_salida),
	      .data_in			(data_in[15:0]),
	      .write			(write),
	      .read0_salida		(read0_salida),
	      .read1_salida		(read1_salida),
	      .read2_salida		(read2_salida),
	      .read3_salida		(read3_salida),
	      .req			(req),
	      .idx			(idx[1:0]),
	      .reset_Lc			(reset_Lc)); 

prueba_s prueba_s(/*AUTOINST*/
		  // Outputs
		  .data_out0_s		(data_out0_s[15:0]),
		  .data_out1_s		(data_out1_s[15:0]),
		  .data_out2_s		(data_out2_s[15:0]),
		  .data_out3_s		(data_out3_s[15:0]),
		  .empty0_salida_s	(empty0_salida_s),
		  .empty1_salida_s	(empty1_salida_s),
		  .empty2_salida_s	(empty2_salida_s),
		  .empty3_salida_s	(empty3_salida_s),
		  .total_s		(total_s[2:0]),
		  .valid_s		(valid_s),
		  // Inputs
		  .clk			(clk),
		  .data_in		(data_in[15:0]),
		  .idx			(idx[1:0]),
		  .read0_salida		(read0_salida),
		  .read1_salida		(read1_salida),
		  .read2_salida		(read2_salida),
		  .read3_salida		(read3_salida),
		  .req			(req),
		  .reset		(reset),
		  .reset_L		(reset_L),
		  .reset_Lc		(reset_Lc),
		  .reset_salida		(reset_salida),
		  .write		(write));


probador probador(/*AUTOINST*/
		  // Outputs
		  .clk			(clk),
		  .reset		(reset),
		  .reset_L		(reset_L),
		  .reset_salida		(reset_salida),
		  .data_in		(data_in[15:0]),
		  .write		(write),
		  .read0_salida		(read0_salida),
		  .read1_salida		(read1_salida),
		  .read2_salida		(read2_salida),
		  .read3_salida		(read3_salida),
		  .req			(req),
		  .idx			(idx[1:0]),
		  .reset_Lc		(reset_Lc),
		  // Inputs
		  .data_out0		(data_out0[15:0]),
		  .data_out1		(data_out1[15:0]),
		  .data_out2		(data_out2[15:0]),
		  .data_out3		(data_out3[15:0]),
		  .empty0_salida	(empty0_salida),
		  .empty1_salida	(empty1_salida),
		  .empty2_salida	(empty2_salida),
		  .empty3_salida	(empty3_salida),
		  .total		(total[2:0]),
		  .valid		(valid),
		  .data_out0_s		(data_out0_s[15:0]),
		  .data_out1_s		(data_out1_s[15:0]),
		  .data_out2_s		(data_out2_s[15:0]),
		  .data_out3_s		(data_out3_s[15:0]),
		  .empty0_salida_s	(empty0_salida_s),
		  .empty1_salida_s	(empty1_salida_s),
		  .empty2_salida_s	(empty2_salida_s),
		  .empty3_salida_s	(empty3_salida_s),
		  .total_s		(total_s[2:0]),
		  .valid_s		(valid_s)); 


endmodule
