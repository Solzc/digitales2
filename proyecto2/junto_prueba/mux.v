module mux( 
    input clk, 
    input reset_L, 
    input [15:0] a,                 // 4-bit input called a
    input [15:0] b,                 // 4-bit input called b
    input [15:0] c,                 // 4-bit input called c
    input [15:0] d,                 // 4-bit input called d
    input [1:0] sel,               // input sel used to select between a,b,c,d
    output reg [15:0] out_mux);         // 4-bit output based on input sel
 
   // This always block gets executed whenever a/b/c/d/sel changes value
   // When that happens, based on value in sel, output is assigned to either a/b/c/d

    always @(posedge clk)begin

    if (reset_L == 0)begin 
        out_mux <= 0;

    end
    end



   always @ (*) begin

        if (sel == 2'b00) begin
            out_mux = a;
        end else if (sel == 2'b01) begin
            out_mux = b;
        end else if (sel == 2'b10) begin
            out_mux = c;
        end else if (sel == 2'b11) begin
            out_mux = d;
        end


    //   case (sel)
    //      2'b00 : out_mux <= a;
    //      2'b01 : out_mux <= b;
    //      2'b10 : out_mux <= c;
    //      2'b11 : out_mux <= d;
    //   endcase

   end
endmodule