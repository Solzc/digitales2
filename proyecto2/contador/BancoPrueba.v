`include "modulo_4FIFOS_s.v"
`include "probador.v"
`include "cmos_cells.v"
`include "modulo_4FIFOS.v"

module BancoPrueba;

// parameter DEPTH=3, MAX_COUNT=3'b111;	
wire [2:0] total, total_s;
wire [1:0] idx;
wire clk, reset, reset_Lc, req, valid, valid_s; 
wire read0, read1, read2, read3, write0, write1, write2, write3;
wire [15:0] data_in0, data_in1, data_in2, data_in3;
wire [15:0] dout0, dout1, dout2, dout3, dout0_s, dout1_s, dout2_s, dout3_s;	//16-bit data output reg


modulo_4FIFOS modulo_4FIFOS(/*AUTOINST*/
			    // Outputs
			    .dout0		(dout0[15:0]),
			    .dout1		(dout1[15:0]),
			    .dout2		(dout2[15:0]),
			    .dout3		(dout3[15:0]),
			    .total		(total[2:0]),
			    .valid		(valid),
			    // Inputs
			    .clk		(clk),
			    .reset		(reset),
			    .data_in0		(data_in0[15:0]),
			    .read0		(read0),
			    .write0		(write0),
			    .data_in1		(data_in1[15:0]),
			    .read1		(read1),
			    .write1		(write1),
			    .data_in2		(data_in2[15:0]),
			    .read2		(read2),
			    .write2		(write2),
			    .data_in3		(data_in3[15:0]),
			    .read3		(read3),
			    .write3		(write3),
			    .req		(req),
			    .idx		(idx[1:0]),
			    .reset_Lc		(reset_Lc));

modulo_4FIFOS_s modulo_4FIFOS_s (/*AUTOINST*/
				 // Outputs
				 .dout0_s		(dout0_s[15:0]),
				 .dout1_s		(dout1_s[15:0]),
				 .dout2_s		(dout2_s[15:0]),
				 .dout3_s		(dout3_s[15:0]),
				 .total_s		(total_s[2:0]),
				 .valid_s		(valid_s),
				 // Inputs
				 .clk			(clk),
				 .data_in0		(data_in0[15:0]),
				 .data_in1		(data_in1[15:0]),
				 .data_in2		(data_in2[15:0]),
				 .data_in3		(data_in3[15:0]),
				 .idx			(idx[1:0]),
				 .read0			(read0),
				 .read1			(read1),
				 .read2			(read2),
				 .read3			(read3),
				 .req			(req),
				 .reset			(reset),
				 .reset_Lc		(reset_Lc),
				 .write0		(write0),
				 .write1		(write1),
				 .write2		(write2),
				 .write3		(write3));

probador probador(/*AUTOINST*/
		  // Outputs
		  .clk			(clk),
		  .reset		(reset),
		  .data_in0		(data_in0[15:0]),
		  .read0		(read0),
		  .write0		(write0),
		  .data_in1		(data_in1[15:0]),
		  .read1		(read1),
		  .write1		(write1),
		  .data_in2		(data_in2[15:0]),
		  .read2		(read2),
		  .write2		(write2),
		  .data_in3		(data_in3[15:0]),
		  .read3		(read3),
		  .write3		(write3),
		  .req			(req),
		  .idx			(idx[1:0]),
		  .reset_Lc		(reset_Lc),
		  // Inputs
		  .dout0		(dout0[15:0]),
		  .dout1		(dout1[15:0]),
		  .dout2		(dout2[15:0]),
		  .dout3		(dout3[15:0]),
		  .total		(total[2:0]),
		  .valid		(valid),
		  .dout0_s		(dout0_s[15:0]),
		  .dout1_s		(dout1_s[15:0]),
		  .dout2_s		(dout2_s[15:0]),
		  .dout3_s		(dout3_s[15:0]),
		  .total_s		(total_s[2:0]),
		  .valid_s		(valid_s));

endmodule
