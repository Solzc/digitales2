module probador(
    output reg clk,
    output reg reset,
    output reg [15:0] data_in0,	//06-bit data output reg
    output reg read0,
    output reg write0,
    output reg [15:0] data_in1,	//16-bit data output reg
    output reg read1,
    output reg write1,
    output reg [15:0] data_in2,	//16-bit data output reg
    output reg read2,
    output reg write2,
    output reg [15:0] data_in3,	//16-bit data output reg
    output reg read3,
    output reg write3,
    output reg req,    //// entradas de contador
    output reg [1:0] idx,
    output reg reset_Lc,
    input [15:0] dout0,
    input [15:0] dout1,
    input [15:0] dout2,
    input [15:0] dout3,
    input [2:0] total,    //salidas de contador
    input valid,
    input [15:0] dout0_s,
    input [15:0] dout1_s,
    input [15:0] dout2_s,
    input [15:0] dout3_s,
    input [2:0] total_s,    //salidas de contador
    input valid_s);	   //flag to indicate that the memory is full
    	
    initial begin 
    $dumpfile("test.vcd");
    $dumpvars;

    reset <= 1'b1;
    reset_Lc <= 1'b0; 
    req <= 0;
    write0 <= 0;
    write1 <= 0;
    write2 <= 0;
    write3 <= 0;
    read0 <= 0;
    read1 <= 0;
    read2 <= 0;
    read3 <= 0;
    @(posedge clk);
        reset <= 1'b0; 
        reset_Lc <= 1'b0; 
        write0 <= 1;
        write1 <= 1;
        write2 <= 1;
        write3 <= 1;
        read0 <= 0;
        read1 <= 0;
        read2 <= 0;
        read3 <= 0;
        data_in0 <= 8'haa;  
        data_in1 <= 8'hab;  
        data_in2 <= 8'hac;  
        data_in3 <= 8'had;  
    @(posedge clk);
        write0 <= 1;
        write1 <= 1;
        write2 <= 1;
        write3 <= 1;
        read0 <= 0;
        read1 <= 0;
        read2 <= 0;
        read3 <= 0;
        data_in0 <= 8'h51;  
        data_in1 <= 8'h52;  
        data_in2 <= 8'h53;  
        data_in3 <= 8'h54;  
    @(posedge clk);
        write0 <= 1;
        write1 <= 1;
        write2 <= 1;
        write3 <= 1;
        read0 <= 0;
        read1 <= 0;
        read2 <= 0;
        read3 <= 0;
        data_in0 <= 8'h66;  
        data_in1 <= 8'h77;  
        data_in2 <= 8'h88;  
        data_in3 <= 8'h99;  
    @(posedge clk);
        write0 <= 1;
        write1 <= 1;
        write2 <= 0;
        write3 <= 1;
        read0 <= 0;
        read1 <= 0;
        read2 <= 0;
        read3 <= 0;
        data_in0 <= 8'hea;  
        data_in1 <= 8'heb;  
        data_in2 <= 8'hec;  
        data_in3 <= 8'hed;  
    @(posedge clk);
        reset_Lc <= 1'b1; // contador empieza a  funcionar 
        write0 <= 0;
        write1 <= 0;
        write2 <= 0;
        write3 <= 0;
        read0 <= 1;
        read1 <= 1;
        read2 <= 1;
        read3 <= 1;
    @(posedge clk);  
        write0 <= 0;
        write1 <= 0;
        write2 <= 0;
        write3 <= 0;
        read0 <= 1;
        read1 <= 1;
        read2 <= 1;
        read3 <= 1; 
    @(posedge clk);
        write0 <= 0;
        write1 <= 0;
        write2 <= 0;
        write3 <= 0;
        read0 <= 1;
        read1 <= 1;
        read2 <= 1;
        read3 <= 1;
    @(posedge clk);
        write0 <= 0;
        write1 <= 0;
        write2 <= 0;
        write3 <= 0;
        read0 <= 1;
        read1 <= 1;
        read2 <= 1;
        read3 <= 1;
    @(posedge clk);
        req <= 1;
        idx <= 3;
     @(posedge clk);
        req <= 0;

    #20 
    $finish;

    end

    initial	clk 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	always	#2 clk 	<= ~clk;


endmodule 