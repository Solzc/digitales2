`include "FIFO.v"
`include "contador.v"

module modulo_4FIFOS (
    input clk,
    input reset,
    input [15:0] data_in0,	//06-bit data input
    input read0,
    input write0,
    input [15:0] data_in1,	//16-bit data input
    input read1,
    input write1,
    input [15:0] data_in2,	//16-bit data input
    input read2,
    input write2,
    input [15:0] data_in3,	//16-bit data input
    input read3,
    input write3,
    input req,    //// entradas de contador
    input [1:0] idx,
    input reset_Lc,
    output [15:0] dout0,
    output [15:0] dout1,
    output [15:0] dout2,
    output [15:0] dout3,
    output [2:0] total,    //salidas de contador
    output valid);

wire empty0, empty1, empty2, empty3;
wire full0, full1, full2, full3;

FIFO FIFO0 (clk,reset,data_in0,read0, write0, dout0,empty0, full0);

FIFO FIFO1 (clk,reset,data_in1,read1, write1, dout1, empty1, full1);

FIFO FIFO2 (clk,reset,data_in2,read2, write2, dout2,empty2, full2);

FIFO FIFO3 (clk,reset,data_in3,read3, write3, dout3,empty3, full3);

contador contador1 (clk, req, idx, reset_Lc, empty0, empty1, empty2, empty3, total, valid);

endmodule 