`include "FIFO.v"
`include "probador.v"
`include "cmos_cells.v"
`include "FIFO_s.v"

module BancoPrueba;

// parameter DEPTH=3, MAX_COUNT=3'b111;	

wire clk, reset,  read, write, empty, full, empty_s, full_s;
wire [15:0] data_in, dout, dout_s;	//16-bit data output reg


FIFO FIFO(/*AUTOINST*/
	  // Outputs
	  .dout				(dout[15:0]),
	  .empty			(empty),
	  .full				(full),
	  // Inputs
	  .clk				(clk),
	  .reset			(reset),
	  .data_in			(data_in[15:0]),
	  .read				(read),
	  .write			(write));


FIFO_s FIFO_s(/*AUTOINST*/
	      // Outputs
	      .dout_s			(dout_s[15:0]),
	      .empty_s			(empty_s),
	      .full_s			(full_s),
	      // Inputs
	      .clk			(clk),
	      .data_in			(data_in[15:0]),
	      .read			(read),
	      .reset			(reset),
	      .write			(write));


probador probador(/*AUTOINST*/
		  // Outputs
		  .clk			(clk),
		  .reset		(reset),
		  .data_in		(data_in[15:0]),
		  .read			(read),
		  .write		(write),
		  // Inputs
		  .dout			(dout[15:0]),
		  .empty		(empty),
		  .full			(full),
		  .dout_s		(dout_s[15:0]),
		  .empty_s		(empty_s),
		  .full_s		(full_s));

endmodule
