module probador(
    output reg clk,
    output reg reset,
    output reg [15:0]data_in,	//16-bit data output reg
    output reg read,
    output reg write,
    input [15:0]dout, //16-bit data input
    input empty, 	   //flag to indicate that the memory is empty
    input full,
    input [15:0]dout_s, //16-bit data input
    input empty_s, 	   //flag to indicate that the memory is empty
    input full_s);	   //flag to indicate that the memory is full
    	

    initial begin 
    $dumpfile("test.vcd");
    $dumpvars;

    reset <= 1'b1;
    write <= 0;
    read <= 0;
    @(posedge clk);
        reset <= 1'b0; 
        write <= 1;
        read <= 0;
        data_in <= 8'haa;  
    @(posedge clk);
        write <= 1;
        read <= 0;
        data_in <= 8'h55;
    @(posedge clk);
        write <= 1;
        read <= 0;
        data_in <= 8'hff;
    @(posedge clk);
        write <= 1;
        read <= 0;
        data_in <= 8'hef;
    @(posedge clk);
        write <= 1;
        read <= 0;
        data_in <= 8'h23;

    @(posedge clk);   //activacion de read y desactivacion de write
        write <= 0;
        read <= 1;
        data_in <= 8'haa;  
    @(posedge clk);
        write <= 0;
        read <= 1;
        data_in <= 8'h55;
    @(posedge clk);
        write <= 0;
        read <= 1;
        data_in <= 8'hff;
    @(posedge clk);
        write <= 0;
        read <= 1;
        data_in <= 8'hef;
    @(posedge clk);
        write <= 0;
        read <= 1;
        data_in <= 8'h23;
    #20 
    $finish;

    end

    initial	clk 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	always	#2 clk 	<= ~clk;

    ///////  CHECKER ///////
    always @(posedge clk) begin
         if (dout != dout_s) begin
            $display("ERROR: SALIDAS DISTINTAS");
        end else begin 
                $display("MODELOS CORRECTOS");
        end
    end

endmodule 