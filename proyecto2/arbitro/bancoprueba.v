`include "prueba.v"
`include "prueba_s.v"
`include "cmos_cells.v"
`include "probador.v"
module bancoprueba(); 

wire clk, reset, write, reset_L, read0, read1, read2, read3, write0, write1, write2, write3,
write0_s, write1_s, write2_s, write3_s;
wire [1:0] dest, s_mux,dest_s, s_mux_s; 
wire [15:0] data_in; 

prueba prueba(/*AUTOINST*/
	      // Outputs
	      .write0			(write0),
	      .write1			(write1),
	      .write2			(write2),
	      .write3			(write3),
	      .dest			(dest[1:0]),
	      .s_mux			(s_mux[1:0]),
	      // Inputs
	      .clk			(clk),
	      .reset			(reset),
	      .data_in			(data_in[15:0]),
	      .write			(write),
	      .reset_L			(reset_L)); 

prueba_s prueba_s(/*AUTOINST*/
		  // Outputs
		  .dest_s		(dest_s[1:0]),
		  .s_mux_s		(s_mux_s[1:0]),
		  .write0_s		(write0_s),
		  .write1_s		(write1_s),
		  .write2_s		(write2_s),
		  .write3_s		(write3_s),
		  // Inputs
		  .clk			(clk),
		  .data_in		(data_in[15:0]),
		  .reset		(reset),
		  .reset_L		(reset_L),
		  .write		(write));



probador probador(/*AUTOINST*/
		  // Outputs
		  .clk			(clk),
		  .reset		(reset),
		  .data_in		(data_in[15:0]),
		  .write		(write),
		  .reset_L		(reset_L),
		  // Inputs
		  .write0		(write0),
		  .write1		(write1),
		  .write2		(write2),
		  .write3		(write3),
		  .dest			(dest[1:0]),
		  .s_mux		(s_mux[1:0]),
		  .write0_s		(write0_s),
		  .write1_s		(write1_s),
		  .write2_s		(write2_s),
		  .write3_s		(write3_s),
		  .dest_s		(dest_s[1:0]),
		  .s_mux_s		(s_mux_s[1:0])); 




endmodule
