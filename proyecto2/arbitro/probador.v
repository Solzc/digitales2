module probador(input write0,
input write1,
input write2,
input write3,
input [1:0] dest, 
input [1:0] s_mux, 
input write0_s,
input write1_s,
input write2_s,
input write3_s,
input [1:0] dest_s, 
input [1:0] s_mux_s, 
output reg clk, 
output reg reset, 
output reg [15:0] data_in, 
output reg write, 
output reg reset_L
); 

    initial begin 
    $dumpfile("out.vcd");
    $dumpvars;

    reset <= 1'b1;
    write <= 0;
    reset_L <= 1'b0; 
    @(posedge clk);
        reset <= 1'b0; 
        reset_L <= 1'b0;
        write <= 1;
        data_in <= 16'haaaa;
     @(posedge clk);
        reset <= 1'b0; 
        reset_L <= 1'b0;
        write <= 1;
        data_in <= 16'h5555;
    @(posedge clk);
        reset <= 1'b0; 
        reset_L <= 1'b0;
        write <= 1;
        data_in <= 16'h0;
    @(posedge clk);
        reset <= 1'b0; 
        reset_L <= 1'b0;
        write <= 1;
        data_in <= 16'hFFFF;
    @(posedge clk);
        write <= 0; 
        reset_L <= 1'b1;
    
     #250 
    $finish;

    end 

    initial	clk 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	always	#2 clk 	<= ~clk;
endmodule
