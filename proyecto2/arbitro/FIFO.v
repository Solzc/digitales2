    module FIFO(clk,reset,data_in,read,write,dout,empty,full);
     
    	input clk;
    	input reset;
    	input [15:0]data_in;	//16-bit data input
    	input read;
    	input write;
     
    	output [15:0]dout; //16-bit data output
    	output empty; 	   //flag to indicate that the memory is empty
    	output full;	   //flag to indicate that the memory is full
    	
    	// parameter DEPTH=3, 7=3'b111;	
    //DEPTH is number of bits, 3 bits thus 2^3=8 memory locations and 7 is the last memory location.
    	
    	reg [15:0]dout;
    	reg empty;
    	reg full;
     
    	/*head is write_pointer and tail is read_pointer*/
     
    	reg [(2):0]tail;	
    // tail(3bits) defines memory pointer location for readata_ing instructions(000 or 001....111)
    	
    	reg [(2):0]head;	
    // head(3bits) defines memory pointer location for writing instructions(000 or 001....111)
    	
    	reg [(2):0]count;	
    // 3 bits count register[000(0),001(1),010(2),....,111(7)]
    	
    	reg [15:0]fifo_mem[0:7]; 
    // fifo memory is having 16 bits data and 8 memory locations
    	
    	reg sr_read_write_empty;			// 1 bit register flag
     
    ///////// WHEN BOTH READatadata_inG AND WRITING BUT FIFO IS EMPTY ////////
     
    	always @(posedge clk)
    		begin
    			if(reset==1)
    			//reset is pressed															
    				sr_read_write_empty <= 0;
    			else if(read==1 && empty==1 && write==1)	
    			//when fifo is empty and read & write both 1
    				sr_read_write_empty <= 1;
    			else
    				sr_read_write_empty <= 0;
    		end
     
    //////////////////////// COUNTER OPERATION ///////////////////////
    		
    	always @(posedge clk)
    		begin
    			if(reset==1)
    //when reset, the fifo is made empty thus count is set to zero
    				count <= 3'b000;		
    			else
    				begin
    					case({read,write})
    					//CASE-1:when not readata_ing or writing	
    						2'b00:	count <= count;				
    								//count remains same
    					//CASE-2:when writing only
    						2'b01:	if(count!=7)			
    									count <= count+1;
    									//count increases
    					//CASE-3:when readata_ing only							
    						2'b10:	if(count!=3'b000)				
    									count <= count-1;							
    									//count decreases
    					//CASE-4
    						2'b11:	if(sr_read_write_empty==1)	
    									count <= count+1;
    //(if) fifo is empty => only write, thus count increases			
    								else
    									count <= count;
    //(else) both read and write takes place, thus no change											
    					//DEFAULT CASE			
    						default: count <= count;
    					endcase
    				end
    		end
     
    ////////////////////// EMPTY AND FULL ALERT /////////////////////
    	
    	// Memory empty signal
    	always @(count)
    		begin
    			if(count==3'b000)
    				empty <= 1;
    			else
    				empty <= 0;
    		end
     
    	// Memory full signal
    	always @(count)
    		begin
    			if(count==7)
    				full <= 1;
    			else
    				full <= 0;
    		end
     
    ///////////// READ AND WRITE POINTER MEMORY LOCATION /////////////
     
    	// Write operation memory pointer
    	always @(posedge clk)
    		begin
    			if(reset==1)
    			//head moved to zero location (fifo is made empty)
    				head <= 3'b000;	
    			else
    				begin
    					if(write==1 && full==0)	
    					//writing when memory is NOT FULL
    						head <= head+1;
    				end
    		end
    	
    	// Read operation memory pointer
    		always @(posedge clk)
    			begin
    				if(reset==1)
    				//tail moved to zero location (fifo is made empty)
    					tail <= 3'b000;	
    				else
    					begin
    						if(read==1 && empty==0)	
    						//readata_ing when memory is NOT ZERO
    							tail <= tail+1;
    					end
    			end
     
    //////////////////// READ AND WRITE OPERATION ////////////////////
     
    	// Write operation
    	always @(posedge clk)
    		//IT CAN WRITE WHEN RESET IS USED AS FULL==0	
    		begin
    			if(write==1 && full==0)
    			//writing when memory is NOT FULL
    				fifo_mem[head] <= data_in;
    			else									
    			//when NOT WRITING
    				fifo_mem[head] <= fifo_mem[head];
    		end
     
    	// Read operation
    	always @(posedge clk)
    		begin
    			// if(reset==1)						
    			// //reset implies output is zero
    			// 	dout <= 16'h0000;
    			if(read==1 && empty==0)	
    			//readata_ing data when memory is NOT EMPTY
    				dout <= fifo_mem[tail];
    			// else
    			// //no change
    			// 	dout <= dout; 
    		end
endmodule
