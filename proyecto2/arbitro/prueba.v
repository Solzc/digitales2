`include "FIFO.v"
`include "arbitro.v"
module prueba(input clk, 
input reset, 
input [15:0] data_in, 
input write, 
input reset_L, 
output write0,
output write1,
output write2,
output write3,
output [1:0] dest, 
output [1:0] s_mux); 

wire empty0, empty1, empty2, empty3, full;
wire [15:0] dout0, dout1, dout2, dout3; 
wire read0, read1, read2, read3;


arbitro arbitro1(clk,dout0,dout1,dout2,dout3,empty0,empty1,empty2,empty3,reset_L,read0,read1,read2,read3,write0,write1,write2,write3,dest,s_mux);

FIFO FIF101(clk, reset, data_in, read0, write, dout0, empty0,full); 
FIFO FIF111(clk, reset, data_in, read1, write, dout1, empty1,full);
FIFO FIF121(clk, reset, data_in, read2, write, dout2, empty2,full);
FIFO FIF131(clk, reset, data_in, read3, write, dout3, empty3,full); 

endmodule

