`include "FIFO.v"
`include "arbitro.v"
`include "demux.v"
`include "mux.v" 
`include "contador.v"

module prueba(input clk, 
input reset, 
input reset_L, 
input reset_salida,
input [15:0] data_in, 
input write, 
input read0_salida,
input read1_salida,
input read2_salida,
input read3_salida,
input req,
input [1:0] idx,
input reset_Lc,
output [15:0] data_out0,
output [15:0] data_out1,
output [15:0] data_out2,
output [15:0] data_out3,
output empty0_salida,
output empty1_salida,
output empty2_salida,
output empty3_salida,
output [2:0] total,
output valid); 

wire empty0, empty1, empty2, empty3, full;
wire [15:0] dout0, dout1, dout2, dout3, out_mux; 
wire read0, read1, read2, read3;
wire [15:0] out_demux0, out_demux1, out_demux2, out_demux3; 
wire [1:0] s_mux, dest;


arbitro arbitro1(clk,dout0,dout1,dout2,dout3,empty0,empty1,empty2,empty3,reset_L,read0,read1,read2,read3,write0,write1,write2,write3,dest,s_mux);

FIFO FIF101(clk, reset, data_in, read0, write, dout0, empty0,full); 
FIFO FIF111(clk, reset, data_in, read1, write, dout1, empty1,full);
FIFO FIF121(clk, reset, data_in, read2, write, dout2, empty2,full);
FIFO FIF131(clk, reset, data_in, read3, write, dout3, empty3,full); 

mux mux1 (dout0, dout1, dout2, dout3, s_mux, out_mux);

demux demux1 (out_mux, dest, out_demux0, out_demux1, out_demux2, out_demux3);

FIFO FIF101_salida(clk, reset_salida, out_demux0, read0_salida, write0, data_out0, empty0_salida,full0_salida); 
FIFO FIF111_salida(clk, reset_salida, out_demux1, read1_salida, write1, data_out1, empty1_salida,full1_salida);
FIFO FIF121_salida(clk, reset_salida, out_demux2, read2_salida, write2, data_out2, empty2_salida,full2_salida);
FIFO FIF131_salida(clk, reset_salida, out_demux3, read3_salida, write3, data_out3, empty3_salida,full3_salida);

contador contador1(clk, req, idx, reset_Lc, write0, write1, write2, write3,total, valid);


endmodule

