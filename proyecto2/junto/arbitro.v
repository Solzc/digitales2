module arbitro(input clk, 
input [15:0]data_out0,
input [15:0]data_out1,
input [15:0]data_out2,
input [15:0]data_out3, 
input empty0, 
input empty1,
input empty2,
input empty3,
input reset_L,
output reg read0,
output reg read1,
output reg read2,
output reg read3,
output reg write0,
output reg write1,
output reg write2,
output reg write3,
output reg [1:0] dest, 
output reg [1:0] s_mux);	  

always @(posedge clk) begin
    write0 <= 0;
    write1 <= 0; 
    write2 <= 0; 
    write3 <= 0; 

 if (empty0 == 1)begin
           if (empty1 == 1)begin
               if(empty2 ==1)begin
                    if (empty3 == 0)begin
                        read3 <= 1; 

                    end 
               end
               else begin
                   read2 <= 1; 

               end
           end
           else begin
               read1 <= 1;  
           end 
        end
        else begin
            read0 <= 1;
        end    

end
//     if (reset_L == 0)begin
//     read0 = 0; 
//     read1 = 0; 
//     read2 = 0; 
//     read3 = 0; 
//     write0 = 0;
//     write1 = 0; 
//     write2 = 0; 
//     write3 = 0; 
//     s_mux = 0; 
// end
// else begin
//     write0 = 0;
//     write1 = 0; 
//     write2 = 0; 
//     write3 = 0; 

//     if (read0 == 1) begin
//         dest[0] = data_out0[0]; 
//         dest[1] = data_out0[1]; 
//         s_mux = 0;
//     end
//     if (read1 == 1) begin
//         dest[0] = data_out1[0]; 
//         dest[1] = data_out1[1]; 
//         s_mux = 1;
//     end
//     if (read2 == 1) begin
//         dest[0] = data_out2[0]; 
//         dest[1] = data_out2[1]; 
//         s_mux = 2;
//     end
//     if (read3 == 1) begin
//         dest[0] = data_out3[0]; 
//         dest[1] = data_out3[1]; 
//         s_mux = 3;
//     end

//     if (dest == 2'b00)begin
//         write0 = 1; 
//     end

//     if (dest == 2'b01)begin
//         write1 = 1; 
//     end

//     if (dest == 2'b10)begin
//         write2 = 1; 
//     end
    
//     if (dest == 2'b11)begin
//         write3 = 1; 
//     end
// end


        
// end

always @(*)begin

    if (reset_L == 0)begin
        read0 = 0; 
        read1 = 0; 
        read2 = 0; 
        read3 = 0; 
        write0 = 0;
        write1 = 0; 
        write2 = 0; 
        write3 = 0; 
        dest = 0; 
        s_mux = 0; 
    end else begin

     

    if (read0 == 1) begin
        dest[0] = data_out0[0]; 
        dest[1] = data_out0[1]; 
        s_mux = 0;
    end
    if (read1 == 1) begin
        dest[0] = data_out1[0]; 
        dest[1] = data_out1[1]; 
        s_mux = 1;
    end
    if (read2 == 1) begin
        dest[0] = data_out2[0]; 
        dest[1] = data_out2[1]; 
        s_mux = 2;
    end
    if (read3 == 1) begin
        dest[0] = data_out3[0]; 
        dest[1] = data_out3[1]; 
        s_mux = 3;
    end

    if (dest == 2'b00)begin
        write0 = 1; 
    end

    if (dest == 2'b01)begin
        write1 = 1; 
    end

    if (dest == 2'b10)begin
        write2 = 1; 
    end
    
    if (dest == 2'b11)begin
        write3 = 1; 
    end
end
end

endmodule
    	
