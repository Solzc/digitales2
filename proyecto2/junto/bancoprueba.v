`include "prueba.v"
`include "probador.v"
module bancoprueba(); 

wire clk, reset, reset_L, reset_salida, write,  read0_salida, read1_salida, read2_salida, read3_salida, empty0_salida, empty1_salida, empty2_salida, empty3_salida;
// wire [1:0] dest, s_mux; 
wire [15:0] data_in,data_out0,data_out1,data_out2,data_out3; 
wire [1:0] idx;
wire [2:0] total;
wire req, reset_Lc, valid;

prueba prueba(/*AUTOINST*/
	      // Outputs
	      .data_out0		(data_out0[15:0]),
	      .data_out1		(data_out1[15:0]),
	      .data_out2		(data_out2[15:0]),
	      .data_out3		(data_out3[15:0]),
	      .empty0_salida		(empty0_salida),
	      .empty1_salida		(empty1_salida),
	      .empty2_salida		(empty2_salida),
	      .empty3_salida		(empty3_salida),
	      .total			(total[2:0]),
	      .valid			(valid),
	      // Inputs
	      .clk			(clk),
	      .reset			(reset),
	      .reset_L			(reset_L),
	      .reset_salida		(reset_salida),
	      .data_in			(data_in[15:0]),
	      .write			(write),
	      .read0_salida		(read0_salida),
	      .read1_salida		(read1_salida),
	      .read2_salida		(read2_salida),
	      .read3_salida		(read3_salida),
	      .req			(req),
	      .idx			(idx[1:0]),
	      .reset_Lc			(reset_Lc)); 


probador probador(/*AUTOINST*/
		  // Outputs
		  .clk			(clk),
		  .reset		(reset),
		  .reset_L		(reset_L),
		  .reset_salida		(reset_salida),
		  .data_in		(data_in[15:0]),
		  .write		(write),
		  .read0_salida		(read0_salida),
		  .read1_salida		(read1_salida),
		  .read2_salida		(read2_salida),
		  .read3_salida		(read3_salida),
		  .req			(req),
		  .idx			(idx[1:0]),
		  .reset_Lc		(reset_Lc),
		  // Inputs
		  .data_out0		(data_out0[15:0]),
		  .data_out1		(data_out1[15:0]),
		  .data_out2		(data_out2[15:0]),
		  .data_out3		(data_out3[15:0]),
		  .empty0_salida	(empty0_salida),
		  .empty1_salida	(empty1_salida),
		  .empty2_salida	(empty2_salida),
		  .empty3_salida	(empty3_salida),
		  .total		(total[2:0]),
		  .valid		(valid)); 


endmodule
