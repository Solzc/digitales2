`include "phy_rx.v"
`include "phy_tx.v"

module phy(
    output [31:0] data_out,
    output valid_out,
    input clk_32f,
    input clk_4f,
    input clk_2f,
    input clk_f,
    input reset_L_bs,
    input reset_L32_8,
    input reset_L_pts,
    input reset_stp,
    input reset_L8_32,
    input reset_L_us,
    input [31:0] data_in,
    input valid_in);

wire data_out_lane0, data_out_lane1;

phy_tx tx1 (data_out_lane0, data_out_lane1, clk_32f,clk_4f,clk_2f,clk_f,reset_L_bs,reset_L32_8,reset_L_pts,data_in,valid_in);

phy_rx rx1 (data_out, valid_out, clk_32f,clk_4f,clk_2f,clk_f,reset_stp, reset_L8_32, reset_L_us, data_out_lane0, data_out_lane1);

endmodule 