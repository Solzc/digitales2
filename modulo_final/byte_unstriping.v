module byte_unstriping(
    input clk_f, 
    input clk_2f, 
    input [31:0] lane_0, 
    input valid_0, 
    input [31:0] lane_1,
    input valid_1, 
    input reset_L_us,
    output reg [31:0] data_out, 
    output reg valid_out); 

reg selector;

always @(posedge clk_2f)begin 
    if (reset_L_us == 1) begin
        selector <= selector + 1; 
        if (valid_0 == 0)begin 
            selector <= 1'b0;
        end 

    end else begin  
        selector <= 1'b0;
        valid_out = 1'b0; 
    end

    if (selector == 1'b0 && valid_0 == 1)begin 
        data_out <= lane_0; 
        valid_out <= 1'b1;
        end 
    else if (selector == 1'b1 && valid_1 == 1)begin
        data_out <= lane_1; 
        valid_out <= 1'b1;  
        end 
end

// always @(*)begin
//     data_out = 32'h0; 
//     valid_out = 1'b0; 
//     // if (selector == 1'b0 && valid_0 == 1)begin 
//     //     data_out = lane_0; 
//     //     valid_out = 1'b1;
//     //     end 
//     // else if (selector == 1'b1 && valid_1 == 1)begin
//     //     data_out = lane_1; 
//     //     valid_out = 1'b1;  
//     //     end 
// end 
 


endmodule 