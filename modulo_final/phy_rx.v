`include "byte_unstriping.v"
`include "a8b_32b.v"
`include "ser_to_par.v"

module phy_rx (
    output [31:0] data_out,
    output valid_out,
    input clk_32f,
    input clk_4f,
    input clk_2f,
    input clk_f,
    input reset_stp,
    input reset_L8_32,
    input reset_L_us,
    input data_in_lane0,
    input data_in_lane1);

/////// serial a paralelo ////////
wire active_0, active_1, valid_out_stp0, valid_out_stp1;
wire [7:0] out_stp_lane0, out_stp_lane1;

/////// 8 a 32 ////////
wire [31:0] out_8_32_lane0, out_8_32_lane1;
wire valid0_8_32, valid1_8_32;

ser_to_par ser_to_par_lane0 (clk_4f, clk_32f, data_in_lane0, reset_stp, active_0, out_stp_lane0,valid_out_stp0);

ser_to_par ser_to_par_lane1 (clk_4f, clk_32f, data_in_lane1, reset_stp, active_1, out_stp_lane1,valid_out_stp1);

a8b_32b a8b_32b_lane0 (out_8_32_lane0, valid0_8_32, out_stp_lane0, valid_out_stp0, reset_L8_32, clk_4f);

a8b_32b a8b_32b_lane1 (out_8_32_lane1, valid1_8_32, out_stp_lane1, valid_out_stp1, reset_L8_32, clk_4f);

byte_unstriping byte_un1 (clk_f, clk_2f, out_8_32_lane0, valid0_8_32, out_8_32_lane1, valid1_8_32, reset_L_us, data_out, valid_out);

endmodule 