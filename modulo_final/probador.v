//`timescale 1s/100us
module probador (
    input [31:0] data_out,
    input valid_out,
    input [31:0] data_out_s,
    input valid_out_s,
    input clk_4f,
    input clk_2f,
    input clk_f,
    output reg clk_32f,
    output reg reset_L,
    output reg reset_L_bs,
    output reg reset_L32_8,
    output reg reset_L_pts,
    output reg reset_stp,
    output reg reset_L8_32,
    output reg reset_L_us,
    output reg [31:0] data_in,
    output reg valid_in);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;

        $display ("\t\t\tclk_32f");
        $monitor($time,"\t%b", clk_32f);

        @(posedge clk_32f);
        valid_in <= 0;
        reset_L <= 0;
        reset_L_bs <= 0;
        reset_L32_8 <= 0;
        reset_L_pts <= 0;
        reset_stp <= 0;
        reset_L8_32 <= 0;
        reset_L_us <= 0;
        @(posedge clk_32f);
        reset_L <= 1;
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        reset_L_bs <= 1;
        reset_L32_8 <= 1;
        reset_L_pts <= 1;
        reset_stp <= 1;
        reset_L8_32 <= 1;
        reset_L_us <= 1;

        @(posedge clk_2f);

        @(posedge clk_2f);

        @(posedge clk_2f);
        data_in <= 32'h12345678; 
        valid_in <= 1;

        @(posedge clk_2f);
        data_in <= 32'h87654321; 

        @(posedge clk_2f);
        data_in <= 32'hABCDE123;

        @(posedge clk_2f);
        data_in <= 32'h45DF23EA;

        @(posedge clk_2f);
        data_in <= 32'h12345678; 

        @(posedge clk_2f);
        data_in <= 32'h87654321; 

        @(posedge clk_2f);
        data_in <= 32'hABCDE123;

        @(posedge clk_2f);
        data_in <= 32'hFEFEFEFE; 

        @(posedge clk_2f);
        data_in <= 32'hABABABAB; 

        @(posedge clk_2f);
        data_in <= 32'hDCDCDCDC; 


    #282 $finish;
    end

    initial	clk_32f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	  always	#2 clk_32f 	<= ~clk_32f;

endmodule 

