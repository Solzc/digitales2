`include "byte_striping.v"
`include "a32b_8b.v"
`include "par_to_ser.v"

module phy_tx (
    output data_out_lane0,
    output data_out_lane1,
    input clk_32f,
    input clk_4f,
    input clk_2f,
    input clk_f,
    input reset_L_bs,
    input reset_L32_8,
    input reset_L_pts,
    input [31:0] data_in,
    input valid_in);

////// byte_stripping //////
wire valid_0_bs, valid_1_bs;
wire [31:0] out_lane0_bs, out_lane1_bs;

////// a32_8b //////
wire [7:0] out_lane0_32_8, out_lane1_32_8;
wire valid_0_31_8, valid_1_31_8;

byte_striping byte_s1 (clk_2f, data_in, valid_in, reset_L_bs, out_lane0_bs, valid_0_bs, out_lane1_bs,valid_1_bs);

a32b_8b a32_8b_lane0 (out_lane0_32_8, valid_0_31_8, out_lane0_bs, valid_0_bs, reset_L32_8, clk_4f);

a32b_8b a32_8b_lane1 (out_lane1_32_8, valid_1_31_8, out_lane1_bs, valid_1_bs, reset_L32_8, clk_4f);

par_to_ser par_to_ser_lane0 (clk_32f, out_lane0_32_8, valid_0_31_8, reset_L_pts, data_out_lane0);

par_to_ser par_to_ser_lane1 (clk_32f, out_lane1_32_8, valid_1_31_8, reset_L_pts, data_out_lane1);

endmodule