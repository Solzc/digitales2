`include "probador.v"
`include "phy.v"
`include "clocks.v"
`include "phy_s.v" 
`include "cmos_cells.v" 

module BancoPrueba;

    wire [31:0] data_out, data_in, data_out_s;
    wire valid_out, clk_32f, clk_4f, clk_2f, clk_f, reset_L_bs, reset_L32_8, reset_L;
    wire reset_L_pts, reset_stp, reset_L8_32, reset_L_us, valid_in, valid_out_s;

	clocks clocks (/*AUTOINST*/
		       // Outputs
		       .clk_4f		(clk_4f),
		       .clk_2f		(clk_2f),
		       .clk_f		(clk_f),
		       // Inputs
		       .clk_32f		(clk_32f),
		       .reset_L		(reset_L));

	phy phy (/*AUTOINST*/
		 // Outputs
		 .data_out		(data_out[31:0]),
		 .valid_out		(valid_out),
		 // Inputs
		 .clk_32f		(clk_32f),
		 .clk_4f		(clk_4f),
		 .clk_2f		(clk_2f),
		 .clk_f			(clk_f),
		 .reset_L_bs		(reset_L_bs),
		 .reset_L32_8		(reset_L32_8),
		 .reset_L_pts		(reset_L_pts),
		 .reset_stp		(reset_stp),
		 .reset_L8_32		(reset_L8_32),
		 .reset_L_us		(reset_L_us),
		 .data_in		(data_in[31:0]),
		 .valid_in		(valid_in));

	phy_s phy_s (/*AUTOINST*/
		     // Outputs
		     .data_out_s		(data_out_s[31:0]),
		     .valid_out_s	(valid_out_s),
		     // Inputs
		     .clk_2f		(clk_2f),
		     .clk_32f		(clk_32f),
		     .clk_4f		(clk_4f),
		     .clk_f		(clk_f),
		     .data_in		(data_in[31:0]),
		     .reset_L32_8	(reset_L32_8),
		     .reset_L8_32	(reset_L8_32),
		     .reset_L_bs	(reset_L_bs),
		     .reset_L_pts	(reset_L_pts),
		     .reset_L_us	(reset_L_us),
		     .reset_stp		(reset_stp),
		     .valid_in		(valid_in));

	probador probador (/*AUTOINST*/
			   // Outputs
			   .clk_32f		(clk_32f),
			   .reset_L		(reset_L),
			   .reset_L_bs		(reset_L_bs),
			   .reset_L32_8		(reset_L32_8),
			   .reset_L_pts		(reset_L_pts),
			   .reset_stp		(reset_stp),
			   .reset_L8_32		(reset_L8_32),
			   .reset_L_us		(reset_L_us),
			   .data_in		(data_in[31:0]),
			   .valid_in		(valid_in),
			   // Inputs
			   .data_out		(data_out[31:0]),
			   .valid_out		(valid_out),
			   .clk_4f		(clk_4f),
			   .clk_2f		(clk_2f),
			   .clk_f		(clk_f));


endmodule 
