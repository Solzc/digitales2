module clocks (
    output reg clk_4f,
    output reg clk_2f,
    output reg clk_f,
    input clk_32f,
    input reset_L);


reg [3:0] contador_4f;
reg [4:0] contador_f;
reg [4:0] contador_2f;


always @ (posedge clk_32f) begin 
    if (reset_L == 0) begin
        contador_4f <= 0;
        contador_f <= 0;
        contador_2f <= 0;
        clk_4f <= 1'b0;
        clk_f <= 1'b0;
        clk_2f <= 1'b0;

    end else begin
        contador_4f <= contador_4f + 1;
        contador_f <= contador_f + 1;
        contador_2f <= contador_2f + 1;

        //////////////// clk_4f ////////////////
        if (contador_4f > 0) begin

            if (contador_4f <= 4) begin
                clk_4f <= 1'b1;
            end else begin
                clk_4f <= 1'b0;
            end
        end

        if (contador_4f == 8)
            contador_4f <= 1;

       //////////////// clk_f /////////////////
        if (contador_f > 0) begin

            if (contador_f <= 16) begin
                clk_f <= 1'b1;
            end else begin
                clk_f <= 1'b0;
            end
        end

        if (contador_f == 32)
            contador_f <= 1;

       //////////////// clk_2f ////////////////
        if (contador_f > 0) begin

            if (contador_2f <= 8) begin
                clk_2f <= 1'b1;
            end else begin
                clk_2f <= 1'b0;
            end
        end

        if (contador_2f == 16)
            contador_2f <= 1;
            
    end
end

endmodule