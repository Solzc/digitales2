`include "par_to_ser.v"
`include "probador.v"
`include "par_to_ser_synth.v"
`include "cmos_cells.v"
`include "clocks.v"
`include "probador_1.v"
module test_bench ();
wire data_out, clk_32f, clk_4f, clk_2f, clk_f,reset_L, valid_in, reset_L_pts,data_out_synth; 
wire [7:0] data_in; 

par_to_ser par_to_ser(/*AUTOINST*/
		      // Outputs
		      .data_out		(data_out),
		      // Inputs
		      .clk_32f		(clk_32f),
		      .data_in		(data_in[7:0]),
		      .valid_in		(valid_in),
		      .reset_L_pts	(reset_L_pts));

par_to_ser_synth par_to_ser_synth(/*AUTOINST*/
				  // Outputs
				  .data_out_synth	(data_out_synth),
				  // Inputs
				  .clk_32f		(clk_32f),
				  .data_in		(data_in[7:0]),
				  .reset_L_pts		(reset_L_pts),
				  .valid_in		(valid_in)); 

probador probador(/*AUTOINST*/
		  // Outputs
		  .data_in		(data_in[7:0]),
		  .valid_in		(valid_in),
		  .reset_L_pts		(reset_L_pts),
		  // Inputs
		  .data_out		(data_out),
		  .clk_32f		(clk_32f),
		  .clk_4f		(clk_4f));

clocks clocks (/*AUTOINST*/
	       // Outputs
	       .clk_4f			(clk_4f),
	       .clk_2f			(clk_2f),
	       .clk_f			(clk_f),
	       // Inputs
	       .clk_32f			(clk_32f),
	       .reset_L			(reset_L));

probador_1 probador_1(/*AUTOINST*/
		      // Outputs
		      .clk_32f		(clk_32f),
		      .reset_L		(reset_L),
		      // Inputs
		      .clk_4f		(clk_4f),
		      .clk_2f		(clk_2f),
		      .clk_f		(clk_f)); 

endmodule 
