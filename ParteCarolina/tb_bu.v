`include "byte_unstriping.v"
`include "probador_bu.v"
`include "byte_unstriping_synth.v"
`include "cmos_cells.v"
`include "clocks.v"
`include "probador_1.v"
module tb_bu(); 
wire clk_32f, clk_4f, clk_2f, clk_f,reset_L, valid_out, valid_1, valid_0, valid_out_s, reset_L_us; 
wire [31:0] data_out, lane_1, lane_0, data_out_s; 

byte_unstriping byte_unstriping(/*AUTOINST*/
				// Outputs
				.data_out	(data_out[31:0]),
				.valid_out	(valid_out),
				// Inputs
				.clk_f		(clk_f),
				.clk_2f		(clk_2f),
				.lane_0		(lane_0[31:0]),
				.valid_0	(valid_0),
				.lane_1		(lane_1[31:0]),
				.valid_1	(valid_1),
				.reset_L_us	(reset_L_us));
byte_unstriping_synth byte_unstriping_synth(/*AUTOINST*/
					    // Outputs
					    .data_out_s		(data_out_s[31:0]),
					    .valid_out_s	(valid_out_s),
					    // Inputs
					    .clk_2f		(clk_2f),
					    .clk_f		(clk_f),
					    .lane_0		(lane_0[31:0]),
					    .lane_1		(lane_1[31:0]),
					    .reset_L_us		(reset_L_us),
					    .valid_0		(valid_0),
					    .valid_1		(valid_1));




probador_bu probador_bu(/*AUTOINST*/
			// Outputs
			.lane_0		(lane_0[31:0]),
			.valid_0	(valid_0),
			.lane_1		(lane_1[31:0]),
			.valid_1	(valid_1),
			.reset_L_us	(reset_L_us),
			// Inputs
			.data_out	(data_out[31:0]),
			.valid_out	(valid_out),
			.data_out_s	(data_out_s[31:0]),
			.valid_out_s	(valid_out_s),
			.clk_2f		(clk_2f),
			.clk_f		(clk_f)); 
clocks clocks(/*AUTOINST*/
	      // Outputs
	      .clk_4f			(clk_4f),
	      .clk_2f			(clk_2f),
	      .clk_f			(clk_f),
	      // Inputs
	      .clk_32f			(clk_32f),
	      .reset_L			(reset_L)); 

probador_1 probador_1(/*AUTOINST*/
		      // Outputs
		      .clk_32f		(clk_32f),
		      .reset_L		(reset_L),
		      // Inputs
		      .clk_4f		(clk_4f),
		      .clk_2f		(clk_2f),
		      .clk_f		(clk_f)); 






endmodule
