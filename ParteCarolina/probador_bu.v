module probador_bu(input [31:0] data_out, 
input valid_out,
input [31:0] data_out_s, 
input valid_out_s,
input clk_2f,
input  clk_f, 
output reg [31:0] lane_0, 
output reg  valid_0, 
output reg [31:0] lane_1, 
output reg  valid_1, 
output reg reset_L_us);
initial begin 
	$dumpfile("out_bu.vcd");
    $dumpvars;

    valid_0 = 1'b0;
    valid_1 = 1'b0;
    lane_0 = 32'h0;
    lane_1 = 32'h0; 
    reset_L_us = 1'b0; 
    
    @(posedge clk_f);
    valid_0 <= 1'b1;
    lane_0 <= 32'hFFFFFFFF;
    reset_L_us <= 1'b1; 
    @(posedge clk_f);
    valid_1 <= 1'b1;
    lane_1 <= 32'hDDDDDDDD;

    @(posedge clk_f);
    valid_0 <= 1'b1;
    lane_0 <= 32'hCCCCCCCC;
    
   @(posedge clk_f);
    valid_1 <= 1'b1;
    lane_1 <= 32'hDDDDDDDD;

    @(posedge clk_f);
    valid_0 <= 1'b0;
    valid_1 <= 1'b0; 

   @(posedge clk_f);
    valid_0 <= 1'b1;
    lane_0 <= 32'h00000003; 
    
     @(posedge clk_f);
    valid_1 <= 1'b1;
    lane_1 <= 32'h00000004;
     $finish;

end
//initial	clk_2f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
//always #2 clk_2f 	<= ~clk_2f;

//initial	clk_f 	<= 1;			// Valor inicial al reloj, sino siempre ser� indeterminado
//always #4 clk_f 	<= ~clk_f;


endmodule