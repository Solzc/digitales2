module probador (
input data_out, 
input clk_32f,
input clk_4f,  
output reg [7:0] data_in, 
output reg valid_in,
output reg reset_L_pts);


 
initial begin 
	$dumpfile("out.vcd");
    $dumpvars;

    valid_in = 1'b0;
    data_in = 8'h0;
    reset_L_pts = 1'b0;

    @(posedge clk_4f);
    data_in <= 8'h0;
    valid_in <= 1'b0;
    reset_L_pts <= 1'b1; 

   
    @(posedge clk_4f);
    data_in <= 8'hFF;
    valid_in <= 1'b1;
    reset_L_pts <= 1'b1; 
   

    @(posedge clk_4f);
    data_in <= 8'hEE;
    valid_in <= 1'b1;
    reset_L_pts <= 1'b1; 
   
    #70
     $finish;

end


//initial	clk_32f 	<= 1;			// Valor inicial al reloj, sino siempre ser� indeterminado
//always #2 clk_32f 	<= ~clk_32f;


endmodule