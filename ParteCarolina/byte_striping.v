module byte_striping(input clk_2f, 
input [31:0] data_in, 
input valid_in, 
input reset_L_bs, 
output reg [31:0] lane_0,
output reg valid_0, 
output reg [31:0] lane_1, 
output reg valid_1 
);

reg selector; 
reg [31:0] aux_0, aux_1; 
reg contador; 

always @(posedge clk_2f)begin 
    if (reset_L_bs == 1'b1)begin 
        selector <= selector + 1; 
        if(selector == 1'b0 && valid_in == 1'b1)begin
            aux_0 <= data_in; 
        end
        else if (selector == 1'b1 && valid_in == 1'b1)begin
            aux_1 <= data_in;
            contador <= 1'b1; 
        end
        if (valid_in == 1'b0)begin
            valid_1 <= 1'b0; 
        end
    end
    else begin
        aux_0 <= 32'h0; 
        aux_1 <= 32'h0; 
        selector <= 1'b0; 
        lane_0 <= 32'h0; 
        lane_1 <= 32'h0; 
        contador <= 1'b0;
        valid_0 <= 1'b0; 
        valid_1 <= 1'b0;  
        end
    
   
end


always @(*)begin   
        valid_1 = 1'b0;  
        valid_0 = 1'b0;
        
    if (selector == 1'b0 && valid_in == 1'b1 )begin
        lane_0 = data_in; 
        valid_0 = 1'b1; 
        lane_1 = aux_1;
        end
    else if (selector == 1'b1 && valid_in == 1'b1)begin
        lane_0 = aux_0; 
        valid_0 = 1'b1; 
        lane_1 = data_in; 
        valid_1 = 1'b1; 
        end
    if (contador == 1'b1)begin
        valid_1 = 1'b1; 
        end
    if (valid_in == 1'b0) begin
        valid_0 = 1'b0; 
        end 
end

always @(valid_in)begin
    if (valid_in == 1) begin 
        selector = 1'b0;
        contador = 1'b0;   
    end
end



endmodule