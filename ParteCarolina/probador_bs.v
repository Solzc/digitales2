module probador_bs(input [31:0] lane_0, 
input valid_0, 
input [31:0] lane_1, 
input valid_1, 
output reg clk_2f, 
output reg [31:0] data_in, 
output reg valid_in
);
initial begin 
	$dumpfile("out_bs.vcd");
    $dumpvars;

    valid_in = 1'b0;
    data_in = 32'h0;


    @(posedge clk_2f);
    data_in <= 32'hFFFFFFFF;
    valid_in <= 1'b1;
   
    @(posedge clk_2f);
    data_in <= 32'hEEEEEEEE;
    valid_in <= 1'b1;

    @(posedge clk_2f);
    data_in <= 32'hDDDDDDDD;
    valid_in <= 1'b1;
    
    @(posedge clk_2f);
    data_in <= 32'hCCCCCCCC;
    valid_in <= 1'b1;

    @(posedge clk_2f);
    data_in <= 32'h0;
    valid_in <= 1'b0;

    @(posedge clk_2f);
    data_in <= 32'h00000003;
    valid_in <= 1'b1;

    @(posedge clk_2f);
    data_in <= 32'h00000004;
    valid_in <= 1'b1;

    #70
     $finish;

end
initial	clk_2f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
always #2 clk_2f 	<= ~clk_2f;

//initial	clk_32f 	<= 1;			// Valor inicial al reloj, sino siempre ser� indeterminado
//always #4 clk_32f 	<= ~clk_32f;


endmodule



