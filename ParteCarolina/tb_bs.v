`include "byte_striping.v"
`include "probador_bs.v"
module tb_bs(); 
wire clk_2f, valid_in, valid_1, valid_0; 
wire [31:0] data_in, lane_1, lane_0; 

byte_striping byte_striping(/*AUTOINST*/
			    // Outputs
			    .lane_0		(lane_0[31:0]),
			    .valid_0		(valid_0),
			    .lane_1		(lane_1[31:0]),
			    .valid_1		(valid_1),
			    // Inputs
			    .clk_2f		(clk_2f),
			    .data_in		(data_in[31:0]),
			    .valid_in		(valid_in));

probador_bs probador_bs(/*AUTOINST*/
			// Outputs
			.clk_2f		(clk_2f),
			.data_in	(data_in[31:0]),
			.valid_in	(valid_in),
			// Inputs
			.lane_0		(lane_0[31:0]),
			.valid_0	(valid_0),
			.lane_1		(lane_1[31:0]),
			.valid_1	(valid_1));





endmodule
