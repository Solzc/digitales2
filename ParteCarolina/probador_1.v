//`timescale 1s/100us
module probador_1 (
    output reg clk_32f,
//output reg clk_4f_1,
    output reg [31:0] data_in,
    output reg valid_0,
    output reg reset_L,
    input clk_4f,
    input clk_2f,
    input clk_f,
    input [7:0] data_out,
    input valid_out);

    initial begin
        $dumpfile("test.vcd");
        $dumpvars;

        $display ("\t\t\tclk_32f");
        $monitor($time,"\t%b", clk_32f);

        @(posedge clk_32f);
        reset_L <= 0;
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        @(posedge clk_32f);
        reset_L <= 1;

        @(posedge clk_32f);
        valid_0 = 1;

        repeat (32) begin
            @(posedge clk_32f);
            data_in <= 32'hABCDE123;
        end

        repeat (32) begin
            @(posedge clk_32f);
            data_in <= 32'h45DF23EA;
        end 

        repeat (32) begin
            @(posedge clk_32f);
            data_in <= 32'h12345678;
        end 

        repeat (32) begin
            @(posedge clk_32f);
            data_in <= 32'h87654321;
        end 

        repeat (32) begin
            @(posedge clk_32f);
            data_in <= 32'hABCDEFAB;
        end 



    $finish;
    end

    initial	clk_32f 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	always	#2 clk_32f 	<= ~clk_32f;

    // initial	clk_4f_1 	<= 1;			// Valor inicial al reloj, sino siempre ser� indeterminado
	// always	#16 clk_4f_1 	<= ~clk_4f_1;


endmodule 

