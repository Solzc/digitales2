`include "ser_to_par.v"
`include "probadorr.v"
`include "clocks.v"
`include "probador_1.v"
module tb();
wire  clk_32f, clk_4f, clk_2f, clk_f,reset_L, data_in, active, valid_out; 
wire [7:0] data2send, data_out;

ser_to_par ser_to_par(/*AUTOINST*/
		      // Outputs
		      .active		(active),
		      .data_out		(data_out[7:0]),
		      .valid_out	(valid_out),
		      // Inputs
		      .clk_4f		(clk_4f),
		      .clk_32f		(clk_32f),
		      .data_in		(data_in),
		      .data2send	(data2send[7:0]));

probadorr probadorr(/*AUTOINST*/
		    // Outputs
		    .data_in		(data_in),
		    .data2send		(data2send[7:0]),
		    // Inputs
		    .data_out		(data_out[7:0]),
		    .active		(active),
		    .valid_out		(valid_out),
		    .clk_4f		(clk_4f),
		    .clk_32f		(clk_32f));

clocks clocks(/*AUTOINST*/
	      // Outputs
	      .clk_4f			(clk_4f),
	      .clk_2f			(clk_2f),
	      .clk_f			(clk_f),
	      // Inputs
	      .clk_32f			(clk_32f),
	      .reset_L			(reset_L)); 


probador_1 probador_1(/*AUTOINST*/
		      // Outputs
		      .clk_32f		(clk_32f),
		      .reset_L		(reset_L),
		      // Inputs
		      .clk_4f		(clk_4f),
		      .clk_2f		(clk_2f),
		      .clk_f		(clk_f));




endmodule 
