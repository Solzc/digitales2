module probadorr(
input [7:0] data_out,
input active, 
input valid_out,
input clk_4f,
input clk_32f,  
output reg  data_in, 
output reg [7:0] data2send);


 
initial begin 
	$dumpfile("out1.vcd");
    $dumpvars;

   data_in = 1'b0; 
    data2send = 8'h0;

    @(posedge clk_4f);
     data2send = 8'hBC;


    @(posedge clk_4f);
     data2send = 8'hBC;

    @(posedge clk_4f);
     data2send = 8'hBC;

     @(posedge clk_4f);
     data2send = 8'hBC;

     @(posedge clk_4f);
     data2send = 8'hFF;
     data_in = 1'b1; 

     @(posedge clk_4f);
     data2send = 8'hEE;
     data_in = 1'b0; 

    @(posedge clk_4f);
     data2send = 8'hEE;
     data_in = 1'b0; 
      @(posedge clk_4f);
     data2send = 8'hEE;
     data_in = 1'b0; 
       @(posedge clk_4f);
     data2send = 8'hEE;
     data_in = 1'b0; 

     @(posedge clk_4f);
     data2send = 8'hBC;
     data_in = 1'b0; 
    #70 
     $finish;

end
//initial	clk 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
//always #20 clk 	<= ~clk;

//initial	clk_32f 	<= 1;			// Valor inicial al reloj, sino siempre ser� indeterminado
//always #5 clk_32f 	<= ~clk_32f;


endmodule