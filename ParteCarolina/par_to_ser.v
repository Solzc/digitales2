module par_to_ser(
input clk_32f,
input [7:0] data_in, 
input valid_in,
input reset_L_pts,
output reg data_out); 


reg [2:0] i; 
reg [7:0] data2send;

always @ (posedge clk_32f) begin 
    if (reset_L_pts == 1'b1) begin
        i <= i-1; 
    if (i == 0)begin
        i <= 7;
        end
//        data_out <= data2send[1*i +:1];
    end
    
    else begin
        i <= 7;
        end
    
    if (valid_in == 1)begin
        data2send <= data_in;
        end 
    else begin 
       data2send <= 8'hBC; 
       end
      
end 


always @(*) begin 
// if (valid_in == 1)begin
//         data2send = data_in;
//         end 
//     else begin 
//        data2send = 8'hBC; 
//        end
    data_out = data2send[1*i +:1];
end



endmodule